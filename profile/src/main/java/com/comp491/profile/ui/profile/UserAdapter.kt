package com.comp491.profile.ui.profile

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.comp491.domain.model.general.UserMeeting
import com.comp491.profile.databinding.LayoutUserItemBinding

class UserAdapter(private val userMeetings: List<UserMeeting>) : RecyclerView.Adapter<UserViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutUserItemBinding.inflate(inflater, parent, false)
        return UserViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(userMeetings[position])
    }

    override fun getItemCount() = userMeetings.size
}

class UserViewHolder(private val binding: LayoutUserItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(userMeeting: UserMeeting) {
        val user = userMeeting.user
        binding.tvName.text = user.getFullName()
        binding.tvCourse.text = userMeeting.meetingName
        val anim = if (user.isStudent()) {
            if (user.isMale()) "student_m_anim.json"
            else "student_f_anim.json"
        } else "teacher_profile_anim.json"
        binding.animPicturePlaceholder.setAnimation(anim)
    }
}
package com.comp491.profile.ui.statistics.charts

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.comp491.profile.R
import com.comp491.profile.databinding.ChartBarBinding
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import java.util.ArrayList

class CustomBarChart @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle) {

    private val binding = ChartBarBinding.inflate(LayoutInflater.from(context), this, true)
    private val chart: BarChart = binding.chart
    private val x = chart.xAxis
    private val y = chart.axisLeft

    private val chartColor = getColor(R.color.ler_purple)
    private val axisColor = getColor(R.color.ler_purple_dark)

    init {
        setupChart()
        disableInteractions()
        styleAxes()
        // hide legend
        chart.legend.isEnabled = false

        chart.invalidate()
    }

    private fun styleAxes() {
        x.position = XAxis.XAxisPosition.BOTTOM
        x.setDrawGridLines(false)
        x.textColor = axisColor
        x.axisLineColor = axisColor
        // hide right y axis
        chart.axisRight.isEnabled = false
        // style left y axis
        y.textColor = axisColor
        y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
        y.axisMinimum = 0f
        y.axisMaximum = 100f
        y.setDrawGridLines(false)
        y.axisLineColor = axisColor
        y.isGranularityEnabled = true
        y.granularity = 1f
        y.valueFormatter = IntFormatter()
    }

    private fun setupChart() {
        chart.setViewPortOffsets(60f, 30f, 30f, 100f)
        chart.setBackgroundColor(Color.TRANSPARENT)
        // no description text
        chart.description.isEnabled = false
        // remove grid
        chart.setDrawGridBackground(false)
    }

    private fun disableInteractions() {
        // disable touch gestures, scaling, and dragging
        chart.setTouchEnabled(false)
        chart.isDragEnabled = false
        chart.setScaleEnabled(false)
        chart.setPinchZoom(false)
    }

    fun setData(numbers: List<Float>, xLabels: List<String>? = null) {
        // setup y axis
        y.axisMinimum = getMinY(numbers)
        y.axisMaximum = getMaxY(numbers)
        val values = ArrayList<BarEntry>()

        for (i in numbers.indices) {
            values.add(BarEntry(i.toFloat(), numbers[i]))
        }

        val set1 = BarDataSet(values, "Data Set")
        set1.color = chartColor

        set1.valueFormatter = IntFormatter()
        set1.setDrawValues(true)

        set1.valueTextSize = 9f
        set1.valueTextColor = axisColor
        val dataSets = ArrayList<IBarDataSet>()
        dataSets.add(set1)
        val data = BarData(dataSets)
        // set data labels
        xLabels?.let { labels ->
            x.valueFormatter = IndexAxisValueFormatter(labels)
        }
        x.labelRotationAngle = 30f
        chart.data = data
        chart.setFitBars(true)

        chart.animateY(1500)
        chart.invalidate()
    }

    private fun getMaxY(values: List<Float>) = ((values.maxOrNull() ?: 0f) + 1).coerceIn(0f..100f)
    private fun getMinY(values: List<Float>) = ((values.minOrNull() ?: 0f) - 1).coerceIn(0f..100f)

    private fun getColor(@ColorRes colorId: Int): Int {
        return ContextCompat.getColor(context, colorId)
    }

    private class IntFormatter : ValueFormatter() {
        override fun getFormattedValue(value: Float) = "${value.toInt()}"
    }

}
package com.comp491.profile.ui.profile

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.comp491.common.ui.base.BaseFragment
import com.comp491.common.ui.dialog.SingleButtonDoubleMessageBottomDialog
import com.comp491.domain.model.general.UserMeeting
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.utils.toGone
import com.comp491.domain.utils.toVisible
import com.comp491.profile.R
import com.comp491.profile.databinding.FragmentProfileBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : BaseFragment<FragmentProfileBinding>() {

    private val viewModel: ProfileViewModel by viewModel()
    private var logoutDialog: SingleButtonDoubleMessageBottomDialog? = null

    override fun setupView() {
        with(binding) {
            tvMyUsersLabel.text = if (viewModel.isUserStudent()) "My Tutors" else "My Students"
            viewModel.getUser()?.let { user ->
                tvName.text = user.getFullName()
                tvEmail.text = user.email
                tvRole.text = user.getType()?.value
                animPicture.setAnimation(viewModel.getPlaceHolderNameForUser(user))
                if (user.isStudent()) btnStats.toVisible()
            }
        }
    }

    override fun setupListeners() {
        binding.btnStats.setOnClickListener {
            navigate(R.id.profile_to_stats)
        }
        binding.btnLogout.setOnClickListener { showLogoutDialog() }
        binding.btnShare.setOnClickListener { shareLernyUrl() }
    }

    private fun shareLernyUrl() {
        val shareIntent = viewModel.createShareIntent()
        startActivity(shareIntent)
    }

    private fun showLogoutDialog() {
        if (logoutDialog == null)
            logoutDialog = SingleButtonDoubleMessageBottomDialog(
                "Logout",
                "Are you sure you wish to logout?",
                "Logout"
            ) { logout() }
        logoutDialog!!.show(childFragmentManager)
    }

    override fun subscribesUI() {
        flowManager().getUserMeetings()
    }

    override fun handleUserMeetings(meetings: List<Meeting>?) {
        meetings?.let {
            val users = viewModel.getUsersFromMeetings(it, viewModel.isUserTutor())
            setupUsersRecycler(users)
        } ?: binding.tvMyUsersLabel.toGone()
    }

    private fun setupUsersRecycler(users: List<UserMeeting>) {
        binding.recyclerUsers.apply {
            adapter = UserAdapter(users)
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
        }
    }

    override fun preViewCreate() {
        super.preViewCreate()
        hideToolbar()
        overrideBackBehavior(
            { backToRoot() },
            ignoresDefaultBehavior = true
        )
    }

}
package com.comp491.profile.ui.statistics

import android.animation.ValueAnimator
import android.view.animation.DecelerateInterpolator
import com.comp491.common.ui.base.BaseFragment
import com.comp491.common.utils.showDialogError
import com.comp491.domain.model.error.AppError
import com.comp491.domain.model.general.Score
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.utils.toVisible
import com.comp491.profile.databinding.FragmentStatisticsBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class StatisticsFragment : BaseFragment<FragmentStatisticsBinding>() {

    private val viewModel: StatisticsViewModel by viewModel()

    override fun setupView() {
//        binding.chartPerformance.setData(grades) todo: put when get scores
    }

    override fun handleUserMeetings(meetings: List<Meeting>?) {
        meetings ?: return handleError(AppError.NullStatisticsError)
        val totalTime = viewModel.getTotalMeetingTimeString(meetings)
        binding.tvTime.text = totalTime
        // meetings this week
        viewModel.getMeetingsPerDay(meetings).let { meetingsPerDay ->
            val numMeetings = meetingsPerDay.map { it.second }
            val days = meetingsPerDay.map { it.first }
            binding.chartEngagement.setData(numMeetings, days)
        }

        viewModel.getAverageScorePerDay(meetings).let { scoreAveragePerDay ->
            val averages = scoreAveragePerDay.map { it.second }
            val days = scoreAveragePerDay.map { it.first }
            binding.chartPerformance.setData(averages, days)
        }

    }

    override fun handleUserScores(scores: List<Score>?) {
        if (scores.isNullOrEmpty()) return handleError(AppError.NoStatisticsError)
        binding.layoutStats.toVisible()
        // show average score
        val avgScore = viewModel.getAverageScore(scores)
        animateScore(avgScore)
        // show max score
        val maxScore = scores.maxOf { it.scorePercentage }
        binding.tvMaxScore.text = "$maxScore%"
    }

    private fun handleError(error: AppError) {
        if (error is AppError.NoStatisticsError)
            binding.groupNoStats.toVisible()
        else {
            showDialogError(error)
            navigateBack()
        }
    }


    private fun animateScore(score: Int) {
        ValueAnimator.ofInt(0, score).apply {
            duration = 2000
            addUpdateListener { anim ->
                val value = anim.animatedValue as Int
                binding.tvScore.text = "$value%"
            }
            interpolator = DecelerateInterpolator()
            start()
        }
        binding.animScore.playAnimation()
    }

    override fun setupListeners() {
    }

    override fun subscribesUI() {
        flowManager().getUserScores(false)
        flowManager().getUserMeetings(false)
    }

    override fun preViewCreate() {
        super.preViewCreate()
        hideToolbar()
    }

}
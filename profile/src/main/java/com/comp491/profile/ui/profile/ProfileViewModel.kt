package com.comp491.profile.ui.profile

import android.content.Intent
import com.comp491.common.Constants
import com.comp491.common.ui.base.BaseViewModel
import com.comp491.domain.model.general.UserMeeting
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.model.user.User
import com.comp491.domain.repos.DataRepository
import com.comp491.domain.utils.isTrue

class ProfileViewModel(dataRepository: DataRepository) : BaseViewModel(dataRepository) {
    fun getPlaceHolderNameForUser(user: User?) = if (user?.isStudent().isTrue()) {
        if (user?.isMale().isTrue()) "student_m_anim.json"
        else "student_f_anim.json"
    } else "teacher_profile_anim.json"

    fun getUsersFromMeetings(meetings: List<Meeting>, getStudents: Boolean) =
        meetings.map { UserMeeting(if (getStudents) it.student else it.tutor, it.meetingTitle) }.groupBy { it.user.email }.values.map {
            val user = it[0].user
            val meetingsNames = it.joinToString(", ") { meeting -> meeting.meetingName }
            UserMeeting(user, meetingsNames)
        }

    fun createShareIntent(): Intent? {
        val sendIntent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, "Check out Lerny: a focused, interactive, and intelligent educational platform - ${Constants.LERNY_URL}")
            type = "text/plain"
        }
        return Intent.createChooser(sendIntent, null)
    }
}
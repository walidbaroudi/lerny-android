package com.comp491.profile.ui.statistics

import com.comp491.common.ui.base.BaseViewModel
import com.comp491.domain.model.general.Score
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.repos.DataRepository
import com.comp491.domain.utils.*

class StatisticsViewModel(dataRepository: DataRepository) : BaseViewModel(dataRepository) {
    fun getAverageScore(scores: List<Score>): Int {
        return scores.map { it.scorePercentage }.average().toInt()
    }

    fun getTotalMeetingTimeString(meetings: List<Meeting>): String {
        val total = meetings.map { it.getLengthInMinutes() }.sum().toInt()
        val hours = total / 60
        val minutes = total % 60
        val hourString = when {
            hours < 1 -> ""
            hours == 1 -> "01 hour "
            else -> "${hours.asMinTwoDigitString()} hours "
        }

        return "${hourString}${minutes.asMinTwoDigitString()} minutes"
    }

    fun getMeetingsPerDay(meetings: List<Meeting>): List<Pair<String, Float>> {
        val pastMeetings = meetings.filter { it.isPast() }
        val meetingsPerDay = getMetricPerDay { day ->
            pastMeetings.filter { isOnSameDay(day, it.startTime) }.size.toFloat()
        }
        return meetingsPerDay
    }


    fun getAverageScorePerDay(meetings: List<Meeting>): List<Pair<String, Float>> {
        val pastMeetings = meetings.filter { it.isPast() }
        val averagesPerDay = getMetricPerDay { day ->
            val meetingsOnDay = pastMeetings.filter { isOnSameDay(day, it.startTime) }
            val average = meetingsOnDay.mapNotNull { it.getScore()?.scorePercentage }.average()
            average.takeIf { it.isNaN().not() }?.toFloat() ?: 0f
        }
        return averagesPerDay
    }

    private fun getMetricPerDay(calculateMetricForDay: (Long) -> Float): List<Pair<String, Float>> {
        val today = now()
        return mutableListOf<Pair<String, Float>>().apply {
            for (i in 0..6) {
                val day = datePlusDay(today, -i)
                val metricThisDay = calculateMetricForDay.invoke(day)
                val dayName = when (i) {
                    0 -> "Today"
                    1 -> "Yesterday"
                    else -> getDayOfWeekName(day)
                }
                add(0, dayName to metricThisDay)
            }
        }
    }


}
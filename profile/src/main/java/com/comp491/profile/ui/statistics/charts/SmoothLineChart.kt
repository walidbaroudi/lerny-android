package com.comp491.profile.ui.statistics.charts

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.comp491.profile.R
import com.comp491.profile.databinding.ChartLineBinding
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import java.util.ArrayList

class SmoothLineChart @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle) {

    private val binding = ChartLineBinding.inflate(LayoutInflater.from(context), this, true)
    private val chart: LineChart = binding.chart
    private val x = chart.xAxis
    private val y = chart.axisLeft

    private val chartColor = getColor(R.color.ler_purple)
    private val axisColor = getColor(R.color.ler_purple_dark)
    private val accentColor = getColor(R.color.ler_pale_purple)

    init {
        setupChart()
        disableInteractions()
        styleAxes()
        // hide legend
        chart.legend.isEnabled = false

        chart.invalidate()
    }

    private fun styleAxes() {
        x.position = XAxis.XAxisPosition.BOTTOM
        x.setDrawGridLines(false)
        x.textColor = axisColor
        x.axisLineColor = axisColor
        // hide right y axis
        chart.axisRight.isEnabled = false
        // style left y axis
        y.textColor = axisColor
        y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
        y.axisMinimum = 0f
        y.axisMaximum = 100f
        y.setDrawGridLines(false)
        y.axisLineColor = axisColor
    }

    private fun setupChart() {
        chart.setViewPortOffsets(60f, 30f, 30f, 50f)
        chart.setBackgroundColor(Color.TRANSPARENT)
        // no description text
        chart.description.isEnabled = false
        // remove grid
        chart.setDrawGridBackground(false)
    }

    private fun disableInteractions() {
        // disable touch gestures, scaling, and dragging
        chart.setTouchEnabled(false)
        chart.isDragEnabled = false
        chart.setScaleEnabled(false)
        chart.setPinchZoom(false)
    }

    fun setData(averages: List<Float>, xLabels: List<String>? = null) {
        // setup y axis
        y.axisMinimum = getMinY(averages)
        y.axisMaximum = getMaxY(averages)
        // make entries
        val values = ArrayList<Entry>()
        for (i in averages.indices) {
            values.add(Entry(i.toFloat(), averages[i]))
        }
        // format dataset
        val set = LineDataSet(values, "DataSet 1")
        set.mode = LineDataSet.Mode.CUBIC_BEZIER
        set.cubicIntensity = 0.2f
        set.setDrawFilled(true)
        set.setDrawCircles(false)
        set.lineWidth = 1.8f
        set.circleRadius = 4f
        set.setCircleColor(chartColor)
        set.highLightColor = chartColor
        set.color = chartColor
        set.fillColor = chartColor
        set.fillAlpha = 0
        set.setDrawHorizontalHighlightIndicator(false)
        set.fillFormatter = IFillFormatter { _, _ -> chart.axisLeft.axisMinimum }

        // create a data object with the data sets
        val data = LineData(set)
        data.setValueTextSize(9f)
        data.setValueTextColor(axisColor)

        // set data
        chart.data = data
        // set data labels
        xLabels?.let { labels ->
            x.valueFormatter = IndexAxisValueFormatter(labels)
        }

        chart.animateXY(750, 1500)
    }

    private fun getMaxY(values: List<Float>) = ((values.maxOrNull() ?: 0f) + 10).coerceIn(0f..100f)
    private fun getMinY(values: List<Float>) = ((values.minOrNull() ?: 0f) - 0).coerceIn(0f..100f)

    private fun getColor(@ColorRes colorId: Int): Int {
        return ContextCompat.getColor(context, colorId)
    }

}
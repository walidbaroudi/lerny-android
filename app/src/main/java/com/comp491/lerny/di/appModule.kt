package com.comp491.lerny.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.comp491.data.backend.RemoteConfig
import com.comp491.data.backend.RetrofitClient
import com.comp491.data.backend.service.LernyBackendService
import com.comp491.data.local.LocalStorage
import com.comp491.data.repos.DataRepositoryImpl
import com.comp491.domain.repos.DataRepository
import com.comp491.home.ui.HomeViewModel
import com.comp491.home.ui.sketch.SketchViewModel
import com.comp491.lerny.ui.MainViewModel
import com.comp491.profile.ui.statistics.StatisticsViewModel
import com.comp491.lerny.ui.login.LoginViewModel
import com.comp491.lerny.ui.register.RegisterTypeViewModel
import com.comp491.lerny.ui.register.RegisterViewModel
import com.comp491.lerny.ui.splash.SplashViewModel
import com.comp491.profile.ui.profile.ProfileViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "local_store")

private val apiUrl: String by lazy { RemoteConfig.getApiUrl() }

val appModule = module {

    // Repositories
    single<DataRepository> { DataRepositoryImpl(get(), get()) }

    // Storage
    single { LocalStorage(androidApplication().dataStore) }

    // Services
    single { LernyBackendService(get(), get(), get()) }
    single { RemoteConfig }

    // API
    single { RetrofitClient(apiUrl) }
    single { get<RetrofitClient>().getUserClient() }
    single { get<RetrofitClient>().getZoomClient() }
    single { get<RetrofitClient>().getMeetingClient() }

    // View Models
    viewModel { MainViewModel(get()) }
    viewModel { SplashViewModel(get()) }
    viewModel { LoginViewModel(get()) }
    viewModel { RegisterViewModel(get()) }
    viewModel { StatisticsViewModel(get()) }
    viewModel { RegisterTypeViewModel(get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { ProfileViewModel(get()) }
    viewModel { SketchViewModel(get()) }
}
package com.comp491.lerny.ui.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.comp491.domain.model.user.UserType
import com.comp491.common.ui.base.BaseViewModel
import com.comp491.domain.repos.DataRepository

class RegisterTypeViewModel(dataRepository: DataRepository) : BaseViewModel(dataRepository) {

    private val _selectedType = MutableLiveData(UserType.USER_STUDENT)
    val selectedType: LiveData<UserType> get() = _selectedType

    fun setUserType(type: UserType) {
        _selectedType.value = type
    }

    fun getUserType() = _selectedType.value

}
package com.comp491.lerny.ui.splash

import com.comp491.common.ui.base.BaseViewModel
import com.comp491.domain.repos.DataRepository

class SplashViewModel(dataRepository: DataRepository) : BaseViewModel(dataRepository) {

    fun isLoggedIn() = dataRepository.isLoggedIn()
}
package com.comp491.lerny.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.comp491.common.managers.FlowHandler
import com.comp491.common.managers.FlowManager
import com.comp491.common.ui.base.BaseViewModel
import com.comp491.data.utils.UiModelState
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.repos.DataRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import com.comp491.data.utils.clear
import com.comp491.domain.model.general.Score

class MainViewModel(dataRepository: DataRepository) : BaseViewModel(dataRepository), FlowManager {

    var flowHandler: FlowHandler? = null

    // Flow manager state flows
    private val _meetings = MutableStateFlow<UiModelState<List<Meeting>>>(UiModelState.None)
    val meetings: StateFlow<UiModelState<List<Meeting>>> get() = _meetings

    private val _scores = MutableStateFlow<UiModelState<List<Score>>>(UiModelState.None)
    val scores: StateFlow<UiModelState<List<Score>>> get() = _scores


    override fun registerHandler(handler: FlowHandler) {
        flowHandler = handler
    }

    override fun unregisterHandler() {
        flowHandler = null
    }

    override fun getUserMeetings(cache: Boolean) {
        flowWrapper(dataRepository.getUserMeetings(), _meetings, cache)
    }

    override fun getUserScores(cache: Boolean) {
        flowWrapper(dataRepository.getUserScores(), _scores, cache)
    }

    override fun invalidateCachedData() {
        _meetings.clear()
        _scores.clear()
    }

    fun clearLocalData() {
        dataRepository.clearLocalData()
    }

}
package com.comp491.lerny.ui.register

import com.comp491.common.Constants
import com.comp491.domain.model.user.UserType
import com.comp491.common.ui.base.BaseFragment
import com.comp491.common.ui.components.LernyInputField
import com.comp491.domain.model.api.UserRegister
import com.comp491.domain.model.user.User
import com.comp491.domain.utils.toGone
import com.comp491.domain.utils.toVisible
import com.comp491.lerny.R
import com.comp491.lerny.databinding.FragmentRegisterBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterFragment : BaseFragment<FragmentRegisterBinding>() {
    private val viewModel: RegisterViewModel by viewModel()

    override fun handleArgs() {

        (arguments?.get(Constants.KEY_USER_TYPE) as? UserType)?.let {
            viewModel.registerType = it
        }
    }

    override fun setupView() {
    }

    override fun setupListeners() {
        binding.btnConfirm.setOnClickListener { validateInputs() }
    }

    private fun validateInputs() {
        clearFieldStyles()
        val errorFields = mutableListOf<LernyInputField>()
        with(binding) {
            // name
            val name = inputName.getText()
            if (viewModel.isValidName(name).not()) errorFields.add(inputName)
            // email
            val email = inputEmail.getText()
            if (viewModel.isValidEmail(email).not()) errorFields.add(inputEmail)
            // password
            val password = inputPassword.getText()
            if (viewModel.isValidPassword(password).not()) errorFields.add(inputPassword)
            val confirm = inputPasswordConfirm.getText()
            if (viewModel.isMatchingPassword(password, confirm).not()) errorFields.add(inputPasswordConfirm)
        }
        if (errorFields.isEmpty())
            confirmRegistration()
        else {
            showSnackBar("Oops! please double check your info", isError = true)
            errorFields.forEach {
                it.showError()
            }
        }
    }

    private fun clearFieldStyles() {
        with(binding) {
            listOf(inputName, inputEmail, inputPassword, inputPasswordConfirm).forEach {
                it.clearError()
            }
        }

    }

    private fun confirmRegistration() {
        with(binding) {
            val register = UserRegister(
                inputEmail.getText(),
                inputPassword.getText(),
                inputName.getText(),
                inputSurname.getText(),
                inputGender.getSelectedText(),
                viewModel.registerType.value
            )
            viewModel.registerUser(register)
        }
    }

    override fun subscribesUI() {
        with(viewModel) {
            observeFlow(
                createUser,
                success = { handleRegisteredUser(it) },
            )
        }
    }

    private fun handleRegisteredUser(user: User) {
        viewModel.setUser(user)
        showSuccess {
            navigate(R.id.register_to_home)
        }
    }

    override fun preViewCreate() {
        super.preViewCreate()
        hideBottomNavigation()
        hideToolbar()
    }

}
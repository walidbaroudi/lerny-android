package com.comp491.lerny.ui.splash

import android.view.animation.DecelerateInterpolator
import com.comp491.common.ui.base.BaseFragment
import com.comp491.common.utils.delayedTask
import com.comp491.domain.utils.runOnMainThread
import com.comp491.lerny.R
import com.comp491.lerny.databinding.FragmentSplashBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.lang.RuntimeException


class SplashFragment : BaseFragment<FragmentSplashBinding>() {

    private val viewModel: SplashViewModel by viewModel()

    override fun onResume() {
        super.onResume()
        delayedTask(2000) {
            runOnMainThread {
                val destination = if (viewModel.isLoggedIn()) R.id.splash_to_home else R.id.splash_to_login
                try {
                    navigate(destination)
                } catch (ignored: IllegalArgumentException) {
                }
            }
        }
    }

    override fun preViewCreate() {
        super.preViewCreate()
        hideBottomNavigation()
        hideToolbar()
    }

    override fun setupView() {
        with(binding.imgLogo) {
            animate().rotation(0f).setInterpolator(DecelerateInterpolator()).alpha(1f).duration = 1500
        }
    }

    override fun setupListeners() {
    }

    override fun subscribesUI() {
    }

}
package com.comp491.lerny.ui

import android.content.Intent
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.comp491.common.Constants
import com.comp491.common.managers.PermissionHandler
import com.comp491.common.managers.PermissionManager
import com.comp491.common.ui.base.BaseActivity
import com.comp491.common.utils.getNavOptions
import com.comp491.common.utils.showDialogError
import com.comp491.domain.utils.checkPermissions
import com.comp491.domain.utils.toGone
import com.comp491.domain.utils.toVisible
import com.comp491.lerny.R
import com.comp491.lerny.databinding.ActivityMainBinding
import com.comp491.lerny.di.appModule
import com.comp491.schedule.meetingModule
import com.comp491.schedule.utils.ZoomPermissions
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules

class MainActivity : BaseActivity<ActivityMainBinding>(), PermissionManager {

    private val viewModel: MainViewModel by viewModel()
    private var permissionsHandler: PermissionHandler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        subscribeUI()
        setupNavigation()
    }

    override fun subscribeUI() {
        with(viewModel) {
            observeFlow(meetings, success = { flowHandler?.handleUserMeetings(it) },
                error = {
                    showDialogError(it)
                    flowHandler?.handleUserMeetings(null)
                })
            observeFlow(scores, success = { flowHandler?.handleUserScores(it) },
                error = {
                    showDialogError(it)
                    flowHandler?.handleUserScores(null)
                })
        }
    }

    override fun logout() {
        Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        }.let {
            resetDependencies()
            viewModel.clearLocalData()
            startActivity(it)
        }
        finish()
    }

    private fun resetDependencies() {
        unloadKoinModules(listOf(appModule, meetingModule))
        loadKoinModules(listOf(appModule, meetingModule))
    }

    override fun setupView() {

    }

    override fun setupListeners() {

        setupNavigation()
    }

    override fun setToolbar() {
        toolbarBorder = findViewById(R.id.toolbar_border)
        toolbar = binding.root.findViewById(R.id.toolbar)
        toolbar?.let {
            toolbarTitle = it.findViewById(R.id.toolbar_title)
            setSupportActionBar(it)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }
    }

    override fun onBackPressed() {
        backBehavior?.let {
            it.invoke()
            return
        }
        if (getNavController()?.currentDestination?.id == R.id.homeFragment) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

    private fun setupNavigation() {
        // setup nav graph
        val navController = getNavController() ?: return// todo: report fatal issue
        navController.setGraph(R.navigation.main_graph)
        // setup bottom navigation
        val bottomNavigationView = binding.bottomNavView
        bottomNavigationView.setupWithNavController(navController)
        // setup toolbar navigation
        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    private fun getNavController() = supportFragmentManager.findFragmentById(R.id.nav_host)?.findNavController()

    override fun showBottomNavigation() {
        binding.bottomNavView.toVisible()
    }

    override fun hideBottomNavigation() {
        binding.bottomNavView.toGone()
    }

    override fun backToRoot(navController: NavController) {
        with(navController) {
            navController.navigate(R.id.home_graph, null, getNavOptions(R.id.home_graph, true))
        }
    }

    override fun goToSchedule(navController: NavController) {
        with(navController) {
            navController.navigate(R.id.schedule_graph, null, getNavOptions(R.id.home_graph, true))
        }
    }

    override fun goToProfile(navController: NavController) {
        with(navController) {
            navController.navigate(R.id.profile_graph, null, getNavOptions(R.id.home_graph, true))
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constants.PERMISSION_ZOOM) {
            val permissionList = ZoomPermissions.getAll().asList()
            val granted = checkPermissions(permissionList)
            permissionsHandler?.handleZoomPermissions(granted)
        }
    }

    override fun checkZoomPermissions(): Boolean {
        val permissionList = ZoomPermissions.getAll()
        if (checkPermissions(permissionList.asList()).not()) {
            ActivityCompat.requestPermissions(
                this, permissionList, Constants.PERMISSION_ZOOM
            )
            return false
        }
        return true
    }

    override fun setPermissionHandler(handler: PermissionHandler) {
        permissionsHandler = handler
    }

    override fun permissionsManager(): PermissionManager = this

    override fun getFlowManager() = viewModel

}
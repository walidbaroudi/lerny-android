package com.comp491.lerny.ui.login

import com.comp491.common.ui.base.BaseFragment
import com.comp491.common.ui.components.LernyInputField
import com.comp491.common.utils.showDialogError
import com.comp491.common.utils.showSnackbarError
import com.comp491.domain.model.error.BackendError
import com.comp491.domain.model.error.LernyError
import com.comp491.domain.model.user.User
import com.comp491.domain.model.user.UserType
import com.comp491.lerny.R
import com.comp491.lerny.databinding.FragmentLoginBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    private val viewModel: LoginViewModel by viewModel()

    override fun setupView() {
    }

    override fun preViewCreate() {
        super.preViewCreate()
        hideToolbar()
        hideBottomNavigation()
    }

    override fun setupListeners() {
        with(binding) {
            btnLogin.setOnClickListener { validateInput() }
            btnRegister.setOnClickListener { navigate(R.id.login_to_register_type) }
        }
    }

    private fun validateInput() {
        val errorFields = mutableListOf<LernyInputField>()
        with(binding) {
            // email
            val email = inputEmail.getText()
            if (viewModel.isValidEmail(email).not()) errorFields.add(inputEmail)
            // password
            val password = inputPassword.getText()
            if (viewModel.isValidPassword(password).not()) errorFields.add(inputPassword)
            if (errorFields.isEmpty())
                login(inputEmail.getText(), inputPassword.getText())
            else {
                showSnackBar("Oops! please double check your info", isError = true)
                errorFields.forEach {
                    it.showError()
                }
            }
        }
    }

    private fun login(email: String, password: String) {
        val typeIndex = binding.viewLoginType.getSelected()
        val type = UserType.values()[typeIndex]
        viewModel.login(email, password, type)
    }

//    navigate(R.id.action_loginFragment_to_home_graph)

    override fun subscribesUI() {
        observeFlow(
            viewModel.authorized,
            success = { handleLogin(it) },
            error = { handleLoginError(it) }
        )
    }

    private fun handleLoginError(error: LernyError) {
        if (error is BackendError.InvalidCredentialsError)
            showSnackbarError(error)
        else
            showDialogError(error)
    }

    private fun handleLogin(user: User) {
        viewModel.setUser(user)
        showSuccess { navigate(R.id.login_to_home) }
    }

}
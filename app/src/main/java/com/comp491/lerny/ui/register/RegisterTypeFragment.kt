package com.comp491.lerny.ui.register

import androidx.core.os.bundleOf
import com.comp491.common.Constants
import com.comp491.domain.model.user.UserType
import com.comp491.common.ui.base.BaseFragment
import com.comp491.common.utils.animateScale
import com.comp491.common.utils.getColor
import com.comp491.lerny.R
import com.comp491.lerny.databinding.FragmentRegisterTypeBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterTypeFragment : BaseFragment<FragmentRegisterTypeBinding>() {

    private val viewModel: RegisterTypeViewModel by viewModel()

    override fun setupView() {
    }

    override fun setupListeners() {
        with(binding) {
            layoutTeacher.setOnClickListener { viewModel.setUserType(UserType.USER_TUTOR) }
            layoutStudent.setOnClickListener { viewModel.setUserType(UserType.USER_STUDENT) }
            btnConfirm.setOnClickListener {
                navigate(
                    R.id.register_type_to_register,
                    bundleOf(Constants.KEY_USER_TYPE to viewModel.getUserType())
                )
            }
        }
    }

    override fun subscribesUI() {
        with(viewModel) {
            selectedType.observe(this@RegisterTypeFragment) { activateAnim(it) }

        }
    }

    override fun preViewCreate() {
        super.preViewCreate()
        hideBottomNavigation()
        hideToolbar()
    }

    private fun activateAnim(type: UserType) {
        val isStudent = type == UserType.USER_STUDENT
        val bigScale = 1.1f
        val smallScale = 0.9f
        with(binding) {
            if (isStudent) {
                tvStudent.animateScale(bigScale)
                tvTeacher.animateScale(smallScale)
                studentAnimation.apply {
                    animateScale(bigScale)
                    resumeAnimation()
                }
                teacherAnimation.apply {
                    animateScale(smallScale)
                    pauseAnimation()
                }
                tvTeacher.setTextColor(getColor(R.color.ler_gray))
                tvStudent.setTextColor(getColor(R.color.ler_dark_text))
            } else {
                tvStudent.animateScale(smallScale)
                tvTeacher.animateScale(bigScale)
                teacherAnimation.apply {
                    animateScale(bigScale)
                    resumeAnimation()
                }
                studentAnimation.apply {
                    animateScale(smallScale)
                    pauseAnimation()
                }
                tvStudent.setTextColor(getColor(R.color.ler_gray))
                tvTeacher.setTextColor(getColor(R.color.ler_dark_text))
            }
        }
    }

}
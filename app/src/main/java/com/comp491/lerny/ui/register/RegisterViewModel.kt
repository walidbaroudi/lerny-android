package com.comp491.lerny.ui.register

import com.comp491.domain.model.user.UserType
import com.comp491.common.ui.base.BaseViewModel
import com.comp491.common.utils.Validator
import com.comp491.data.utils.UiModelState
import com.comp491.domain.model.api.UserRegister
import com.comp491.domain.model.user.User
import com.comp491.domain.repos.DataRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class RegisterViewModel(dataRepository: DataRepository) : BaseViewModel(dataRepository) {

    var registerType = UserType.USER_STUDENT
    private val _createUser = MutableStateFlow<UiModelState<User>>(UiModelState.None)
    val createUser: StateFlow<UiModelState<User>> get() = _createUser

    fun isValidEmail(email: String) = Validator.isValidEmail(email)

    fun isValidPassword(password: String) = Validator.isValidPassword(password)

    fun isMatchingPassword(password: String, confirm: String) = Validator.isMatchingPassword(password, confirm)
    fun isValidName(name: String?) = Validator.isValidName(name)
    fun isValidAge(age: String?) = Validator.isValidAge(age)

    fun registerUser(register: UserRegister) {
        flowWrapper(dataRepository.registerUser(register), _createUser)
    }

    fun setUser(user: User) {
        dataRepository.setUser(user)
    }
}
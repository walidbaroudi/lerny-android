package com.comp491.lerny.ui.login

import com.comp491.domain.model.user.UserType
import com.comp491.common.ui.base.BaseViewModel
import com.comp491.common.utils.Validator
import com.comp491.data.utils.UiModelState
import com.comp491.domain.model.user.User
import com.comp491.domain.repos.DataRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class LoginViewModel(dataRepository: DataRepository) : BaseViewModel(dataRepository) {


    private val _authorized = MutableStateFlow<UiModelState<User>>(UiModelState.None)
    val authorized: StateFlow<UiModelState<User>> get() = _authorized


    fun isValidEmail(email: String) = Validator.isValidEmail(email)
    fun isValidPassword(password: String) = Validator.isValidPassword(password)

    fun login(email: String, password: String, type: UserType) {
        flowWrapper(dataRepository.loginStudent(email, password), _authorized)
    }

    fun setUser(user: User) {
        dataRepository.setUser(user)
    }
}
package com.comp491.lerny

import android.app.Application
import com.comp491.lerny.di.appModule
import com.comp491.schedule.meetingModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext.startKoin

class LernyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@LernyApplication)
            modules(listOf(appModule, meetingModule))
        }
    }
}
package com.comp491.schedule.create

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.widget.DatePicker
import android.widget.TimePicker
import com.comp491.common.ui.base.BaseFragment
import com.comp491.common.ui.components.LernyInputField
import com.comp491.common.utils.Validator
import com.comp491.common.utils.showDialogError
import com.comp491.domain.model.api.MeetingCreate
import com.comp491.domain.model.error.AppError
import com.comp491.domain.model.general.SimpleDate
import com.comp491.domain.model.general.SimpleTime
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.utils.*
import com.comp491.schedule.databinding.FragmentNewMeetingBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewMeetingFragment : BaseFragment<FragmentNewMeetingBinding>(), TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    private val viewModel: NewMeetingViewModel by viewModel()
    private lateinit var fields: List<LernyInputField>
    private var questionsDialog: MeetingQuestionsDialog? = null


    override fun setupView() {
        with(binding) {
            fields = listOf(editDateRoot, editDurationRoot, editStudentRoot, editTimeRoot, editMeetingNameRoot)
        }
    }

    override fun setupListeners() {
        with(binding) {
            editTimeRoot.apply {
                setText(viewModel.selectedTime.getTimeString())
                setOnClickListener { showTimePicker() }
            }
            editDateRoot.apply {
                setText(viewModel.selectedDate.getDateString())
                setOnClickListener { showDatePicker() }
            }
            btnConfirm.setOnClickListener {
                clearErrors()
                validateInput()
            }

            btnPrepareQuestions.setOnClickListener {
                showQuestionsDialog()
            }
        }
    }

    private fun showQuestionsDialog() {
        if (questionsDialog == null) questionsDialog = MeetingQuestionsDialog(viewModel.preparedQuestions)
        questionsDialog!!.show(childFragmentManager)
    }

    private fun validateInput() {
        val errorFields = mutableListOf<LernyInputField>()
        fields.forEach {
            if (Validator.isValidInput(it.getText()).not()) errorFields.add(it)
        }
        if (errorFields.isEmpty()) {
            collectMeeting()?.let { submitMeeting(it) }
        } else {
            errorFields.forEach { it.showError() }
        }
    }

    private fun clearErrors() {
        fields.forEach { it.clearError() }
    }

    private fun collectMeeting(): MeetingCreate? {
        return with(binding) {
            val tutorEmail = viewModel.getUser()?.email ?: run {
                showDialogError(AppError.NoUserFoundError)
                return null
            }
            val startTime = getTimeMillis(viewModel.selectedDate, viewModel.selectedTime)
            val endTime = datePlusMinute(startTime, editDurationRoot.getText().toInt())
            MeetingCreate(
                editMeetingNameRoot.getText(),
                tutorEmail,
                editStudentRoot.getText(),
                startTime,
                endTime,
                viewModel.preparedQuestions
            )
        }
    }

    private fun submitMeeting(meeting: MeetingCreate) {
        viewModel.createMeeting(meeting)
    }

    override fun subscribesUI() {
        observeFlow(
            viewModel.meeting,
            success = { handleCreateMeeting() },
            showSuccess = true
        )
    }

    private fun handleCreateMeeting() {
        flowManager().invalidateCachedData()
        navigateBack()
    }

    private fun showTimePicker() {
        val hour = viewModel.selectedTime.hour
        val minute = viewModel.selectedTime.minute
        val dialog = TimePickerDialog(requireContext(), this, hour, minute, false)
        dialog.show()
    }

    private fun showDatePicker() {
        val day = viewModel.selectedDate.day
        val month = viewModel.selectedDate.month
        val year = viewModel.selectedDate.year
        val dialog = DatePickerDialog(requireContext(), this, year, month, day)
        dialog.show()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        val newTime = SimpleTime(hourOfDay, minute)
        viewModel.selectedTime = newTime
        binding.editTimeRoot.setText(newTime.getTimeString())
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val newDate = SimpleDate(dayOfMonth, month, year)
        viewModel.selectedDate = newDate
        binding.editDateRoot.setText(newDate.getDateString())
    }

}
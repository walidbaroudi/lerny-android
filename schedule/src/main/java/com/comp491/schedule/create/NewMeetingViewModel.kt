package com.comp491.schedule.create

import com.comp491.common.ui.base.BaseViewModel
import com.comp491.data.utils.UiModelState
import com.comp491.domain.model.api.MeetingCreate
import com.comp491.domain.model.general.SimpleDate
import com.comp491.domain.model.general.SimpleTime
import com.comp491.domain.model.meeting.Question
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.repos.DataRepository
import com.comp491.domain.utils.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class NewMeetingViewModel(dataRepository: DataRepository) : BaseViewModel(dataRepository) {

    var selectedDate = SimpleDate(getDayOfMonth(now()), getMonth(now()), getYear(now()))
    var selectedTime = SimpleTime(getHour(now()), getMinute(now()))

    var preparedQuestions = mutableListOf<Question>()

    private val _meeting = MutableStateFlow<UiModelState<Meeting>>(UiModelState.None)
    val meeting: StateFlow<UiModelState<Meeting>> get() = _meeting

    fun createMeeting(meeting: MeetingCreate) {
        flowWrapper(dataRepository.createMeeting(meeting), _meeting)
    }

}
package com.comp491.schedule.create

import androidx.recyclerview.widget.LinearLayoutManager
import com.comp491.common.ui.base.BaseBottomSheetDialog
import com.comp491.domain.model.meeting.Question
import com.comp491.schedule.databinding.DialogMeetingQuestionsBinding
import com.comp491.schedule.ui.meeting.NewQuestionDialog

class MeetingQuestionsDialog(questions: MutableList<Question>, questionClickedCallback: ((Question) -> Unit)? = null) :
    BaseBottomSheetDialog<DialogMeetingQuestionsBinding>(),
    NewQuestionDialog.QuestionConfirmedListener {

    private val questionsAdapter = MeetingQuestionsAdapter(questions) { dismissOnCallback(it, questionClickedCallback) }

    override fun setupView() {
        binding.recyclerQuestions.apply {
            adapter = questionsAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun setupListeners() {
        binding.btnAddQuestion.setOnClickListener {
            NewQuestionDialog(this).show(childFragmentManager)
        }
    }

    override fun onQuestionConfirmed(question: Question) {
        questionsAdapter.addQuestion(question)
    }

    private fun dismissOnCallback(question: Question, callback: ((Question) -> Unit)?): Unit? {
        return callback?.let {
            it.invoke(question)
            dismiss()
        }
    }
}
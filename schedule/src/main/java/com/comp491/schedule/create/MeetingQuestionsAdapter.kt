package com.comp491.schedule.create

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.comp491.domain.model.meeting.Question
import com.comp491.domain.model.meeting.QuestionType
import com.comp491.schedule.R
import com.comp491.schedule.databinding.CellQuestionEntryBinding

class MeetingQuestionsAdapter(private val questions: MutableList<Question>, private val questionClickedAction: ((Question) -> Unit)?) :
    RecyclerView.Adapter<MeetingQuestionsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeetingQuestionsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CellQuestionEntryBinding.inflate(inflater, parent, false)
        return MeetingQuestionsViewHolder(binding, questionClickedAction)
    }

    override fun onBindViewHolder(holder: MeetingQuestionsViewHolder, position: Int) {
        holder.bind(questions[position])
    }

    override fun getItemCount() = questions.size

    fun addQuestion(question: Question) {
        questions.add(question)
        notifyItemInserted(questions.size)
    }
}

class MeetingQuestionsViewHolder(
    private val binding: CellQuestionEntryBinding,
    private val onQuestionClicked: ((Question) -> Unit)?
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(question: Question) {
        with(binding) {
            tvQuestion.text = question.questionText
            if (question.type == QuestionType.MC) tvAnswers.text = question.answers.joinToString(",") { "'$it'" }
            val typeIcon = if(question.type == QuestionType.MC) R.drawable.ic_multi_choice else R.drawable.ic_true_false
            imgType.setImageResource(typeIcon)
            tvTime.text = "${question.time}s"
            onQuestionClicked?.let { callback ->
                root.setOnClickListener { callback.invoke(question) }
            }
        }
    }

}
package com.comp491.schedule

import com.comp491.schedule.create.NewMeetingViewModel
import com.comp491.schedule.ui.meeting.MeetingViewModel
import com.comp491.schedule.ui.schedule.ScheduleViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val meetingModule = module {

    viewModel { ScheduleViewModel(get()) }
    viewModel { NewMeetingViewModel(get()) }
    viewModel { MeetingViewModel(get()) }
}
package com.comp491.schedule.domain

import android.content.Context
import android.util.Log
import com.comp491.domain.utils.runAsync
import com.comp491.domain.utils.indexOfMax
import com.comp491.schedule.utils.toBitmap
import com.comp491.schedule.ml.HandGestureModel
import com.comp491.schedule.model.Gesture
import com.comp491.schedule.model.Quarter
import com.comp491.schedule.model.Quarter.*
import com.comp491.schedule.utils.get2DCoordinates
import com.comp491.schedule.utils.getLandmarksByteBuffer
import com.comp491.schedule.utils.isMultipleHands
import com.google.mediapipe.solutions.hands.Hands
import com.google.mediapipe.solutions.hands.HandsOptions
import com.google.mediapipe.solutions.hands.HandsResult
import org.tensorflow.lite.DataType
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import us.zoom.sdk.*
import java.nio.ByteBuffer

private const val TAG = "HandsAnalyzer"

class HandAnalyzer(private val user: ZoomVideoSDKUser, private val listener: HandResultListener, private val gestureConfirmationFrames: Int, context: Context) :
    ZoomVideoSDKRawDataPipeDelegate {

    private val handsModel: Hands
    private val gestureModel: HandGestureModel

    private var started = false

    private var confirmationCounter = 0
    private var multiHandCounter = 0
    private var confirmationGestures = listOf<Gesture>()

    init {
        // initialize hands model
        val handsOptions = HandsOptions.builder()
            .setStaticImageMode(true)
            .setMaxNumHands(2)
            .setRunOnGpu(true)
            .build()
        handsModel = Hands(context, handsOptions)
        handsModel.setResultListener { handleHandsModelResult(it) }
        // initialize gesture model
        gestureModel = HandGestureModel.newInstance(context)
    }

    private fun handleHandsModelResult(result: HandsResult) {
        // multiple hands in frame
        Log.d("LANDMARKS", "getLandmarksByteBuffer: HANDS = ${result.multiHandLandmarks().size}")
        if (result.isMultipleHands())
            return handleMultiHands()
        multiHandCounter = 0
        // get current hand position quarter
        var quarter = NONE
        result.get2DCoordinates()?.let { coordinates ->
            findQuarter(coordinates).let { quarter = it }
        }
        listener.onQuarterSelected(quarter)
        // get gesture
        if (quarter.isValid().not()) return
        val frame = result.inputBitmap()
        val landmarksBuffer = result.getLandmarksByteBuffer(frame.width, frame.height)
        sendLandmarksToGestureModel(landmarksBuffer, quarter)
    }

    private fun handleMultiHands() {
        if (multiHandCounter < 4) {
            multiHandCounter++
        } else {
            listener.onMultipleHandsDetected()
            multiHandCounter = 0
        }
    }

    private fun sendLandmarksToGestureModel(landmarksBuffer: ByteBuffer?, quarter: Quarter) {
        // Creates inputs for reference.
        landmarksBuffer?.let { buffer ->
            val inputFeature = TensorBuffer.createFixedSize(intArrayOf(1, 21, 2), DataType.FLOAT32)
            inputFeature.loadBuffer(buffer)

            // Runs model inference and gets result.
            val outputs = gestureModel.process(inputFeature)
            val outputFeature = outputs.outputFeature0AsTensorBuffer
            val probabilities = outputFeature.floatArray
            probabilities.indexOfMax()?.let { index ->
                val gesture = Gesture.values()[index]
                listener.onGestureSelected(gesture, quarter)
                checkConfirmation(gesture, quarter)

            }
        }
    }

    private fun checkConfirmation(gesture: Gesture, quarter: Quarter) {
        if (confirmationCounter == -1) return
        if (gesture in confirmationGestures) {
            if (confirmationCounter < gestureConfirmationFrames)
                confirmationCounter++
            else {
                confirmationCounter = -1
                listener.onConfirmationGestureSelected(quarter, gesture)
            }
            Log.d(TAG, "checkConfirmation: CONFIRMATION COUNTER = $confirmationCounter, GESTURE = ${gesture.name}")
        } else {
            confirmationCounter = 0
        }
    }

    private fun findQuarter(coordinates: com.comp491.schedule.utils.FloatPoint): Quarter {
        val x = coordinates.x
        val y = coordinates.y
        val firstHalf = 0.0..0.5
        val secondHalf = 0.5..1.0
        return when {
            (x in secondHalf) && (y in firstHalf) -> TOP_RIGHT // first quarter
            x in firstHalf && y in firstHalf -> TOP_LEFT // second quarter
            x in firstHalf && y in secondHalf -> BOT_LEFT // third quarter
            x in secondHalf && y in secondHalf -> BOT_RIGHT // fourth quarter
            else -> NONE
        }
    }

    fun start(confirmationGestures: List<Gesture>) {
        confirmationCounter = 0
        this.confirmationGestures = confirmationGestures
        user.videoPipe.subscribe(ZoomVideoSDKVideoResolution.VideoResolution_360P, this).let {
            if (it == ZoomVideoSDKErrors.Errors_Success)
                started = true
        }
    }

    fun stop() {
        user.videoPipe.unSubscribe(this).let {
            if (it == ZoomVideoSDKErrors.Errors_Success)
                started = false
        }
    }

    fun close() {
        stop()
//        gestureModel.close() todo: fix
    }

    override fun onRawDataFrameReceived(frame: ZoomVideoSDKVideoRawData?) {
        runAsync {
            frame?.toBitmap()?.let {
                handsModel.send(it)
            } ?: run { Log.e(TAG, "onRawDataFrameReceived: NULL BITMAP") }
        }
    }

    override fun onRawDataStatusChanged(status: ZoomVideoSDKRawDataPipeDelegate.RawDataStatus?) {
        Log.i(TAG, "onRawDataStatusChanged: ${status?.name}")
    }

}

interface HandResultListener {
    fun onQuarterSelected(quarter: Quarter)
    fun onGestureSelected(gesture: Gesture, quarter: Quarter)
    fun onConfirmationGestureSelected(quarter: Quarter, gesture: Gesture)
    fun onMultipleHandsDetected()
}
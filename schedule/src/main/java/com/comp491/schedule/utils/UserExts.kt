package com.comp491.schedule.utils

import us.zoom.sdk.ZoomVideoSDKUser

fun ZoomVideoSDKUser.getInitials() = userName.split(" ").map { it[0] }.joinToString("")
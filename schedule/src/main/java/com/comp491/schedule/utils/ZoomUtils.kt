package com.comp491.schedule.utils

import android.graphics.*
import org.opencv.android.Utils
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.imgproc.Imgproc
import us.zoom.sdk.ZoomVideoSDKVideoRawData
import java.nio.ByteBuffer


fun ZoomVideoSDKVideoRawData.toBitmap(): Bitmap {
    val width = streamWidth
    val height = streamHeight

    val yPlane = getyBuffer()
    val uPlane = getuBuffer()
    val vPlane = getvBuffer()

    val yuvBytes = getYUVBytes(yPlane, uPlane, vPlane, width, height)

    val rgbaMat = Mat()
    val yuvMat = Mat(height + height / 2, width, CvType.CV_8UC1)
    yuvMat.put(0, 0, yuvBytes)
    Imgproc.cvtColor(yuvMat, rgbaMat, Imgproc.COLOR_YUV2RGBA_I420, 4)
    return rgbaMat.toBitmap().flipHorizontal()
}

private fun Mat.toBitmap(): Bitmap {
    val bmp = Bitmap.createBitmap(cols(), rows(), Bitmap.Config.ARGB_8888)
    Utils.matToBitmap(this, bmp)
    return bmp
}

private fun getYUVBytes(yPlane: ByteBuffer, uPlane: ByteBuffer, vPlane: ByteBuffer, width: Int, height: Int): ByteArray {
    val yuvBytes = ByteArray(width * (height + height / 2))

    yPlane.get(yuvBytes, 0, width * height)
    var offset = width * height
    uPlane.get(yuvBytes, offset, width * height / 4)
    offset += width * height / 4
    vPlane.get(yuvBytes, offset, width * height / 4)

    return yuvBytes
}


fun Bitmap.flipHorizontal(): Bitmap {
    return Bitmap.createScaledBitmap(this, width * -1, height, true)
}

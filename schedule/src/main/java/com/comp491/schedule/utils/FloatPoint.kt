package com.comp491.schedule.utils

data class FloatPoint(val x: Float, val y: Float)

package com.comp491.schedule.utils

import android.Manifest

object ZoomPermissions {
    const val CAMERA = Manifest.permission.CAMERA
    const val RECORD_AUDIO = Manifest.permission.RECORD_AUDIO
    const val READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE
    const val BLUETOOTH = Manifest.permission.BLUETOOTH_CONNECT

    fun getAll() = arrayOf(CAMERA, RECORD_AUDIO, READ_EXTERNAL_STORAGE, BLUETOOTH)
        .asList().toMutableList().apply {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.S)
                remove(BLUETOOTH)
        }.toTypedArray()
}



package com.comp491.schedule.utils

import android.util.Log
import com.google.mediapipe.solutions.hands.HandLandmark
import com.google.mediapipe.solutions.hands.HandsResult
import java.nio.ByteBuffer
import java.nio.ByteOrder

fun HandsResult.get2DCoordinates(): FloatPoint? {
    multiHandLandmarks().takeIf { it.isNotEmpty() }?.get(0).takeIf { it?.landmarkList?.isNullOrEmpty() == false }?.let {
        val indexFingerLandmark = it.landmarkList[HandLandmark.MIDDLE_FINGER_MCP]
        return FloatPoint(indexFingerLandmark.x, indexFingerLandmark.y)
    }
    return null
}

fun HandsResult.getLandmarksByteBuffer(frameWidth: Int, frameHeight: Int): ByteBuffer? {
    multiHandLandmarks().firstOrNull()?.takeIf { it.landmarkList?.isNullOrEmpty() == false }?.let {
        val landmarks = it.landmarkList.map { landmark -> listOf(landmark.x * frameWidth, landmark.y * frameHeight) }
        Log.w("TAG", "num of landmarks: ${landmarks.size}")
        val buffer = ByteBuffer.allocateDirect(4 * 21 * 2)
        buffer.order(ByteOrder.nativeOrder())
        landmarks.forEach { mark ->
            mark.forEach { coordinate ->
                buffer.putFloat(coordinate)
            }
        }
        return buffer
    }

    return null
}

fun HandsResult.isMultipleHands() = multiHandLandmarks().size > 1
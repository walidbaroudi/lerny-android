package com.comp491.schedule.model

enum class Quarter {
    TOP_RIGHT,
    TOP_LEFT,
    BOT_LEFT,
    BOT_RIGHT,
    NONE;

    fun isValid() = this != NONE
}
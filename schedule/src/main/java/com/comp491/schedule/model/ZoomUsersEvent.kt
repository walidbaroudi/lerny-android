package com.comp491.schedule.model

import us.zoom.sdk.ZoomVideoSDKUser

data class ZoomUsersEvent(
    val users: List<ZoomVideoSDKUser>?,
    val event: UserEvent
)

enum class UserEvent {
    USER_JOIN,
    USER_LEAVE,
    AUDIO_CHANGE,
    VIDEO_CHANGE
}
package com.comp491.schedule.model

enum class Gesture(val isConfirm: Boolean) {
    OKAY(false),
    PEACE(false),
    THUMBS_UP(true),
    THUMBS_DOWN(false),
    CALL_ME(false),
    STOP(false),
    ROCK(false),
    LIVE_LONG(false),
    FIST(false),
    SMILE(false)
}
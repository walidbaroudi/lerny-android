package com.comp491.schedule.ui.meeting

import com.comp491.common.utils.delayedTask
import com.comp491.domain.model.meeting.Answer
import com.comp491.domain.utils.DelayableAction
import com.comp491.domain.utils.runOnMainThread
import com.comp491.domain.utils.toGone
import com.comp491.domain.utils.toVisible
import com.comp491.schedule.model.Quarter
import com.comp491.domain.model.meeting.Question
import com.comp491.domain.model.meeting.QuestionType
import com.comp491.schedule.model.Gesture
import com.comp491.schedule.ui.components.QuizView

class QuizHandler(private val quizView: QuizView, private val onQuizEndedCallback: (Answer) -> Unit) {

    private var currentQuestionTimer: DelayableAction? = null
    private var currentQuestion: Question? = null

    fun startQuestion(question: Question) {
        currentQuestion = question
        quizView.setQuestion(question)
        currentQuestionTimer = DelayableAction(question.getQuestionTimeMillis(),
            endAction = { endQuiz(true, 1f) },
            tickAction = { quizView.setQuizTimer((it / 1000).toInt()) }).also { it.invoke() }
        quizView.toVisible()
    }

    fun confirmAnswer(quarter: Quarter, gesture: Gesture) {
        currentQuestion ?: return
        currentQuestionTimer?.cancel()
        val answer = if (currentQuestion!!.type == QuestionType.MC) {
            quizView.confirmAnswer(quarter)
            quarter.ordinal
        } else {
            quizView.confirmAnswer(gesture)
            if (gesture == Gesture.THUMBS_UP) 1 else 0
        }
        endQuiz(false, 2.5f, answer)
    }

    fun selectAnswer(quarter: Quarter) {
        if (quizView.questionEnded.not()) quizView.hideMultiHandWarning()
        if (currentQuestion?.type != QuestionType.MC) return
        quizView.selectQuarter(quarter)
    }

    private fun endQuiz(isTimeOut: Boolean, hideAfter: Float, answerIndex: Int = -1) {
        onQuizEndedCallback.invoke(Answer(answerIndex))
        if (isTimeOut)
            quizView.showTimeout()

        val timeMillis = (hideAfter * 1000).toInt()
        delayedTask(timeMillis) {
            runOnMainThread {
                quizView.reset()
                quizView.toGone()
                currentQuestion = null
            }
        }
    }

    fun showMultipleHandsWarning() {
        quizView.apply {
            showMultiHandWarning()
            selectQuarter(Quarter.NONE)
        }
    }

}
package com.comp491.schedule.ui.components

import android.view.View
import androidx.fragment.app.FragmentManager
import com.comp491.common.R
import com.comp491.common.databinding.DialogNoConnectionBinding
import com.comp491.common.ui.base.BaseDialogFragment
import com.comp491.common.utils.delayedTask
import com.comp491.domain.model.meeting.AnswerEvaluation
import com.comp491.schedule.databinding.DialogAnswerBinding

// Using in the ActivityExt
class AnswerDialog(val answer: String, private val onEvaluationCompleted: (AnswerEvaluation) -> Unit) : BaseDialogFragment<DialogAnswerBinding>() {


    override fun setupView() {
        isCancelable = false
        binding.lblAnswer.text = answer
    }

    override fun setupListeners() {
        binding.btnCorrect.setOnClickListener { sendEvaluation(true) }
        binding.btnIncorrect.setOnClickListener { sendEvaluation(false) }
    }

    private fun sendEvaluation(correct: Boolean) {
        onEvaluationCompleted.invoke(AnswerEvaluation(correct))
        dismiss()
    }

    fun safeDismiss() {
        if (isVisible.not()) return
        dismiss()
    }
}

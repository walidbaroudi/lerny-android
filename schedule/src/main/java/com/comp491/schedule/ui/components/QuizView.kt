package com.comp491.schedule.ui.components

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.comp491.common.utils.animateBackgroundTint
import com.comp491.common.utils.animateScale
import com.comp491.common.utils.animateTint
import com.comp491.common.utils.setScale
import com.comp491.schedule.databinding.LayoutQuizViewBinding
import com.comp491.schedule.model.Quarter
import com.comp491.domain.model.meeting.Question
import com.comp491.domain.model.meeting.QuestionType
import com.comp491.domain.utils.*
import com.comp491.schedule.R
import com.comp491.schedule.model.Gesture

class QuizView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle) {

    private var selectedQuarter = 0

    private val binding = LayoutQuizViewBinding.inflate(LayoutInflater.from(context), this, true)

    private val quarters: List<View>
    private val answerLabels: List<TextView>
    private val trueFalseViews: List<View>

    private lateinit var questionType: QuestionType
    var questionEnded = false

    init {
        with(binding) {
            quarters = listOf(quarterTopRight, quarterTopLeft, quarterBotLeft, quarterBotRight)
            answerLabels = listOf(tvTopRight, tvTopLeft, tvBotLeft, tvBotRight)
            trueFalseViews = listOf(imgTrue, imgFalse)
        }
        reset()
        binding.tvQuestion.setOnClickListener {
            showQuarters(false)
        }
    }

    fun reset() {
        questionEnded = false
        runOnMainThread {
            resetMc()
            resetTf()
            binding.groupQuestion.setBackgroundTintColor(getColor(R.color.white))
        }
    }

    private fun resetTf() {
        binding.imgFalse.apply {
            setScale(1f)
            backgroundTintList = ColorStateList.valueOf(getColor(R.color.ler_ultra_light_gray))
            imageTintList = ColorStateList.valueOf(getColor(R.color.ler_fail))
        }
        binding.imgTrue.apply {
            setScale(1f)
            backgroundTintList = ColorStateList.valueOf(getColor(R.color.ler_ultra_light_gray))
            imageTintList = ColorStateList.valueOf(getColor(R.color.ler_success))
        }
        showTrueFalse(false)
    }

    private fun resetMc() {
        quarters.forEach {
            it.isSelected = false
            it.alpha = 0.7f
        }
        showQuarters(false)
    }

    fun hideMultiHandWarning() {
        with(binding) {
            runOnMainThread {
                showCurrentGroup(true)
                tvWarning.toGone()
                groupQuestion.groupToVisible(binding.root)
                quarters.forEach { it.alpha = 0.7f }
            }
        }
    }

    fun showMultiHandWarning() {
        with(binding) {
            runOnMainThread {
                showCurrentGroup(false)
                groupQuestion.groupToGone(binding.root)
                tvWarning.toVisible()
            }
        }
    }

    fun setQuestion(question: Question) {
        binding.tvQuestion.text = question.questionText
        binding.groupQuestion.groupToVisible(binding.root)
        questionType = question.type
        if (question.type.isMultiChoice()) {
            question.answers.zip(answerLabels) { answer, label -> label.text = answer }
        }
        showCurrentGroup(true)
    }

    fun selectQuarter(quarter: Quarter) {
        val index = quarter.ordinal
        if (index == selectedQuarter) return
        quarters.forEach { it.isSelected = false }
        if (quarter.isValid())
            quarters[index].isSelected = true

        selectedQuarter = index
    }

    fun setQuizTimer(seconds: Int) {
        binding.tvTimer.text = seconds.toString()
    }

    fun confirmAnswer(quarter: Quarter) {
        questionEnded = true
        runOnMainThread {
            val quarterView = quarters[quarter.ordinal]
            binding.groupQuestion.groupToGone(binding.root)
            quarters.forEach { if (it != quarterView) it.toGone() }
            quarterView.animate().alpha(1f).duration = 300
        }
    }

    fun confirmAnswer(gesture: Gesture) {
        questionEnded = true
        runOnMainThread {
            when (gesture) {
                Gesture.THUMBS_DOWN -> {
                    binding.imgFalse.apply {
                        animateTint(getColor(R.color.ler_fail), getColor(R.color.ler_ultra_light_gray))
                        animateBackgroundTint(getColor(R.color.ler_ultra_light_gray), getColor(R.color.ler_fail))
                        binding.groupQuestion.animateBackgroundTint(getColor(R.color.ler_ultra_light_gray), getColor(R.color.ler_fail))
                        animateScale(1.5f, 1000)
                    }
                    binding.imgTrue.apply {
                        animateScale(0f, 1000)
                    }
                }
                Gesture.THUMBS_UP -> {
                    binding.imgTrue.apply {
                        animateTint(getColor(R.color.ler_success), getColor(R.color.ler_ultra_light_gray))
                        animateBackgroundTint(getColor(R.color.ler_ultra_light_gray), getColor(R.color.ler_success))
                        binding.groupQuestion.animateBackgroundTint(getColor(R.color.ler_ultra_light_gray), getColor(R.color.ler_success))
                        animateScale(1.5f, 1000)
                    }
                    binding.imgFalse.apply {
                        animateScale(0f, 1000)
                    }
                }
                else -> {}
            }
        }
    }

    private fun showQuarters(show: Boolean) {
        if (show) {
            quarters.forEach {
                it.alpha = 0.7f
                it.toVisible()
            }
        } else
            quarters.forEach { it.toGone() }
    }

    private fun showTrueFalse(show: Boolean) {
        if (show)
            trueFalseViews.forEach { it.toVisible() }
        else
            trueFalseViews.forEach { it.toGone() }
    }

    private fun showCurrentGroup(show: Boolean) {
        if (questionType.isMultiChoice())
            showQuarters(show)
        else
            showTrueFalse(show)
    }

    private fun getColor(@ColorRes colorId: Int): Int {
        return ContextCompat.getColor(context, colorId)
    }

    fun showTimeout() { // todo: show animation
        Toast.makeText(context, "Question timed out", Toast.LENGTH_SHORT).show()
    }
}
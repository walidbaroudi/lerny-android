package com.comp491.schedule.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.comp491.domain.model.session.Meeting
import com.comp491.schedule.databinding.LayoutSessionWideItemBinding

class ScheduledSessionAdapter(
    private var meetings: List<Meeting>,
    private val isCurrentStudent: Boolean,
    private val joinCallback: (Meeting) -> Unit
) : RecyclerView.Adapter<ScheduledSessionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduledSessionViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutSessionWideItemBinding.inflate(inflater, parent, false)
        return ScheduledSessionViewHolder(binding, joinCallback, isCurrentStudent)
    }

    override fun onBindViewHolder(holder: ScheduledSessionViewHolder, position: Int) {
        holder.bind(meetings[position])
    }

    override fun getItemCount() = meetings.size

    fun updateMeetings(meetings: List<Meeting>) {
        this.meetings = meetings
        notifyDataSetChanged()
    }
}

class ScheduledSessionViewHolder(
    private val binding: LayoutSessionWideItemBinding,
    private val joinCallback: (Meeting) -> Unit,
    private val isCurrentStudent: Boolean
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(meeting: Meeting) {
        with(binding) {
            tvUserName.text = if (isCurrentStudent) meeting.tutor.getFullName() else meeting.student.getFullName()
            tvSessionName.text = meeting.meetingTitle
            tvTime.text = meeting.time.getTimeRangeString()
            btnJoin.setOnClickListener { joinCallback.invoke(meeting) }
        }
    }

}
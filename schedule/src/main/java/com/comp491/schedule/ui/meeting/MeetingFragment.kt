package com.comp491.schedule.ui.meeting

import android.util.Log
import com.comp491.common.Constants
import com.comp491.common.ui.base.BaseFragment
import com.comp491.common.ui.dialog.DoubleButtonDoubleMessageDialog
import com.comp491.common.ui.dialog.SingleButtonDoubleMessageBottomDialog
import com.comp491.common.utils.delayedTask
import com.comp491.common.utils.showDialogError
import com.comp491.domain.model.error.AppError
import com.comp491.domain.model.error.ZoomError
import com.comp491.domain.model.meeting.Answer
import com.comp491.domain.model.meeting.AnswerEvaluation
import com.comp491.domain.utils.*
import com.comp491.schedule.R
import com.comp491.schedule.databinding.FragmentMeetingBinding
import com.comp491.schedule.domain.HandAnalyzer
import com.comp491.schedule.domain.HandResultListener
import com.comp491.schedule.model.*
import com.comp491.domain.model.meeting.Question
import com.comp491.domain.model.session.Meeting
import com.comp491.schedule.create.MeetingQuestionsDialog
import com.comp491.schedule.ui.components.AnswerDialog
import com.comp491.schedule.utils.getInitials
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.opencv.android.OpenCVLoader
import us.zoom.sdk.*
import java.lang.Exception

class MeetingFragment : BaseFragment<FragmentMeetingBinding>(), HandResultListener, NewQuestionDialog.QuestionConfirmedListener {

    private val viewModel: MeetingViewModel by viewModel()
    private var handAnalyzer: HandAnalyzer? = null // todo: move inside quiz handler?
    private lateinit var quizHandler: QuizHandler
    private var questionsDialog: MeetingQuestionsDialog? = null
    private var leaveMeetingDialog: SingleButtonDoubleMessageBottomDialog? = null
    private var questionReceivedDialog: DoubleButtonDoubleMessageDialog? = null

    override fun handleArgs() {
        with(viewModel) {
            val meeting = arguments?.getParcelable<Meeting>(Constants.KEY_MEETING) ?: return run {
                val error = AppError.CorruptedMeetingError
                showDialogError(error)
                error.sendToCrashlytics()
                navigateBack()
            }

            meetingQuestions = meeting.questions.toMutableList()
            meetingName = meeting.meetingTitle.toSafeString("Unknown")
            meetingId = meeting.getId()
            meeting.endpoint?.let { topic ->
                meetingEndpoint = topic
                getToken(topic)
            }
        }
    }

    private fun getToken(topic: String) {
        viewModel.generateToken(topic)
    }

    override fun setupView() {
        if (!OpenCVLoader.initDebug()) {
            // todo: report error
            Log.e(TAG, "setupView: Failed to load OpenCV")
        } else {
            binding.btnAskQuestion.isEnabled = true
        }
        showQuestionButton(true)
    }

    private fun handleQuizEnd(answer: Answer) {
        viewModel.submitAnswer(answer)
        requireActivity().runOnUiThread {
            binding.groupMeetingControl.toVisible()
            showQuestionButton(true)
            showAltVideo(true)
        }
    }

    private fun showQuestionButton(show: Boolean) {
        if (viewModel.isUserStudent()) return
        binding.btnAskQuestion.apply { if (show) toVisible() else toGone() }
    }

    override fun preViewCreate() {
        super.preViewCreate()
        hideToolbar()
        hideBottomNavigation()
        overrideBackBehavior({ showLeaveMeetingDialog() }, ignoresDefaultBehavior = true, permanent = true)
    }

    private fun leaveSession() {
        if (viewModel.sessionJoined.value.notTrue()) {
            navigateBack()
        } else {
            getZoom().leaveSession(true)
        }
    }

    override fun setupListeners() {
        getZoom().addListener(viewModel)
        with(binding) {
            btnDisconnect.setOnClickListener { showLeaveMeetingDialog() }
            btnAudio.setOnClickListener { toggleAudio() }
            btnVideo.setOnClickListener { toggleVideo() }
            altVideoView.setOnClickListener { viewModel.switchMainUser() }
            btnAskQuestion.setOnClickListener { showMeetingQuestionsDialog() }
            quizHandler = QuizHandler(viewQuiz) { handleQuizEnd(it) }
        }
    }

    private fun showMeetingQuestionsDialog() {
        if (questionsDialog == null) questionsDialog = MeetingQuestionsDialog(viewModel.meetingQuestions) { viewModel.sendQuestion(it) }
        questionsDialog!!.show(childFragmentManager)
    }

    private fun showLeaveMeetingDialog() {
        if (leaveMeetingDialog == null) leaveMeetingDialog = SingleButtonDoubleMessageBottomDialog(
            "Leave Meeting",
            "Are you sure you wish to leave the meeting?",
            "Leave",
        ) { leaveSession() }

        leaveMeetingDialog!!.show(childFragmentManager)
    }

    private fun startQuiz(question: Question) {
        viewModel.setMyselfMainUser()
        binding.groupMeetingControl.toGone()
        showQuestionButton(false)
        showAltVideo(false)
        quizHandler.startQuestion(question)
        handAnalyzer?.start(viewModel.confirmationGesturesForQuestionType(question.type))
    }

    private fun showAltVideo(show: Boolean) {
        if (show)
            viewModel.getOtherUser()?.let {
                binding.altVideoView.toVisible()
            }
        else
            binding.altVideoView.toGone()
    }

    private fun toggleAudio() {
        when {
            viewModel.isSelfAudioDisconnected() -> {
                unMuteSelfAudio(true)
            }
            viewModel.isSelfMuted() -> {
                unMuteSelfAudio()
            }
            else -> {
                muteSelfAudio()
            }
        }
    }

    private fun unMuteSelfAudio(connectAudio: Boolean = false) {
        with(getZoom().audioHelper) {
            if (connectAudio) startAudio()
            unMuteAudio(viewModel.mySelf)
        }
    }

    private fun muteSelfAudio() {
        getZoom().audioHelper.muteAudio(viewModel.mySelf)
    }

    private fun setAudioToggleUi(muted: Boolean) {
        val image = if (muted) R.drawable.ic_mic_off else R.drawable.ic_mic_on
        binding.btnAudio.setImageResource(image)
    }

    private fun toggleVideo() {
        with(getZoom().videoHelper) {
            if (viewModel.isSelfVideoOn()) stopVideo() else startVideo()
        }
    }

    private fun setVideoToggleUi(videoOff: Boolean) {
        val image = if (videoOff) R.drawable.ic_video_off else R.drawable.ic_video_on
        binding.btnVideo.setImageResource(image)
        binding.tvPlaceholder.apply { if (videoOff) toVisible() else toGone() }
    }

    private fun setPlaceholdersVisibility(selfVideoOn: Boolean? = null, otherVideoOn: Boolean? = null) {
        val self = selfVideoOn ?: viewModel.mySelf?.videoStatus?.isOn
        val other = otherVideoOn ?: viewModel.getOtherUser()?.videoStatus?.isOn
        val myselfMain = viewModel.isMyselfMain()
        if (myselfMain) {
            self?.let { binding.tvPlaceholder.apply { if (it) toGone() else toVisible() } }
            other?.let { showAltPlaceHolder(it.not()) }
        } else {
            self?.let { showAltPlaceHolder(it.not()) }
            other?.let { binding.tvPlaceholder.apply { if (it) toGone() else toVisible() } }
        }
    }

    private fun setOrientation() { // fix mirroring effect
        with(getZoom().videoHelper) {
            switchCamera()
            switchCamera()
        }
    }

    override fun subscribesUI() {
        val owner = this
        with(viewModel) {
            sessionJoined.observe(owner) { handleSessionState(it) }
            errorCode.observe(owner) { handleZoomError(it) }
            users.observe(owner) { handleUsersEvent(it) }
            mainUser.observe(owner) { handleMainUserChange() }
            observeFlow(token, success = { joinSession(it) })

            // quiz
            receivedQuestion.observe(owner) { handleQuestion(it) }
            receivedAnswer.observe(owner) { handleAnswer(it) }
            receivedEvaluation.observe(owner) { handleEvaluation(it) }
        }
    }

    private fun handleQuestion(question: Question) {
        if (viewModel.isSelfVideoOn())
            startQuiz(question)
        else showQuestionReceivedDialog(question)
    }

    private fun showQuestionReceivedDialog(question: Question) {
        if (questionReceivedDialog == null) questionReceivedDialog = DoubleButtonDoubleMessageDialog(
            "Question Received",
            "You need to enable video to answer the question",
            "Enable video",
            "Cancel",
            positiveAction = {
                toggleVideo()
                startQuiz(question)
            },
            cancellable = false
        )

        questionReceivedDialog!!.show(childFragmentManager)
    }

    private fun handleAnswer(answer: Answer) {
        val answerText = viewModel.lastQuestion?.getAnswer(answer.index).toSafeString("?")
        AnswerDialog(answerText) { eval -> sendEvaluation(eval) }.show(childFragmentManager)
    }

    private fun handleEvaluation(evaluation: AnswerEvaluation) {
        viewModel.recordEvaluation(evaluation)
        val anim = if (evaluation.isCorrect) "correct_anim.json" else "incorrect_anim.json"
        binding.animResult.apply {
            setAnimation(anim)
            toVisible()
            playAnimation()
            delayedTask(3000) { runOnMainThread { this.toGone() } }
        }
    }

    private fun sendEvaluation(evaluation: AnswerEvaluation) {
        viewModel.sendEvaluation(evaluation)
    }


    private fun handleMainUserChange() {
        // check if mySelf is main
        val isMyselfMain = viewModel.isMyselfMain()
        // set placeholders
        val placeholders = viewModel.getPlaceHolderTexts()
        placeholders.first?.let { binding.tvPlaceholder.text = it }
        placeholders.second?.let { binding.tvAltPlaceholder.text = it }
        // show muted indicator
        setMutedIndicators(isMyselfMain)
        // subscribe video
        val other = viewModel.getOtherUser()
        if (isMyselfMain) {
            unsubscribeUserVideo(binding.mainVideoView, other)
            unsubscribeUserVideo(binding.altVideoView, viewModel.mySelf)
            subscribeUserVideo(binding.mainVideoView, viewModel.mySelf)
            subscribeUserVideo(binding.altVideoView, other)
        } else {
            unsubscribeUserVideo(binding.mainVideoView, viewModel.mySelf)
            unsubscribeUserVideo(binding.altVideoView, other)
            subscribeUserVideo(binding.mainVideoView, other)
            subscribeUserVideo(binding.altVideoView, viewModel.mySelf)
        }

        setPlaceholdersVisibility()
    }

    private fun setMutedIndicators(myselfMain: Boolean) {
        val otherMuted = viewModel.getOtherUser()?.audioStatus?.isMuted ?: return
        if (myselfMain) {
            binding.imgMuted.toGone()
            binding.imgAltMuted.apply { if (otherMuted) toVisible() else toGone() }
        } else {
            binding.imgAltMuted.toGone()
            binding.imgMuted.apply { if (otherMuted) toVisible() else toGone() }
        }
    }

    private fun joinSession(token: String) { // todo: move stuff to viewModel
        if (token.isBlank()) return showDialogError(ZoomError.ZoomBlankToken)
        showProgress(true)
        val params = ZoomVideoSDKSessionContext().apply {
            sessionName = viewModel.meetingEndpoint.toSafeString()
            userName = viewModel.getUser()?.getFullName().toSafeString()
            this.token = token
//            // create initial audio options
            ZoomVideoSDKAudioOption().apply {
                connect = true
                mute = false
                audioOption = this
            }
//            // create initial video options
            ZoomVideoSDKVideoOption().apply {
                localVideoOn = true
                videoOption = this
            }
        }

        val session = getZoom().joinSession(params)
        if (session.isNull()) {
            showDialogError(Exception("Could not join session")) { navigateBack() }
        }
    }

    private fun handleSessionState(joined: Boolean?) {
        joined ?: return
        if (joined.not()) { // session left
            if (viewModel.isUserStudent()) viewModel.submitMeetingScore()
            flowManager().invalidateCachedData()
            navigateBack()
        } else { // session joined
            binding.tvSessionName.text = viewModel.meetingName
            getZoom().session.mySelf.apply {
                viewModel.mySelf = this
                viewModel.switchMainUser()
                subscribeUserVideo(binding.mainVideoView, this)
                handAnalyzer = HandAnalyzer(this, this@MeetingFragment, 10, requireContext())
                setOrientation()
                setControlButtonsUi()
                binding.tvPlaceholder.text = this.getInitials()
                showProgress(false)
            }
        }
    }

    private fun setControlButtonsUi() {
        setAudioToggleUi(viewModel.isSelfMuted().or(viewModel.isSelfAudioDisconnected()))
        setVideoToggleUi(viewModel.isSelfVideoOn().not())
    }

    private fun handleUsersEvent(usersEvent: ZoomUsersEvent?) {
        usersEvent ?: return
        val user = usersEvent.users?.firstOrNull() ?: return
        when (usersEvent.event) {
            UserEvent.USER_JOIN -> {
                showAltVideo(true)
                subscribeUserVideo(binding.altVideoView, user)
                binding.tvAltPlaceholder.text = user.getInitials()
            }
            UserEvent.USER_LEAVE -> {
                showAltVideo(false)
                viewModel.switchMainUser()
                clearOtherUserViews()
            }
            UserEvent.AUDIO_CHANGE -> {
                handleAudioChange(usersEvent.users)
            }
            UserEvent.VIDEO_CHANGE -> {
                handleVideoChange(usersEvent.users)
            }
        }
    }

    private fun clearOtherUserViews() {
        with(binding) {
            showAltPlaceHolder(false)
            imgAltMuted.toGone()
        }
    }

    private fun handleAudioChange(users: List<ZoomVideoSDKUser>?) {
        users?.forEach { user ->
            if (viewModel.isMyself(user))
                setAudioToggleUi(user.audioStatus.isMuted)
            else {
                setMutedIndicators(viewModel.isMyselfMain())
            }
        }
    }

    private fun handleVideoChange(users: List<ZoomVideoSDKUser>?) {
        var selfVideoOn: Boolean? = null
        var otherVideoOn: Boolean? = null
        users?.forEach { user ->
            if (viewModel.isMyself(user)) {
                selfVideoOn = user.videoStatus.isOn
                setVideoToggleUi(selfVideoOn!!.not())
            } else {
                otherVideoOn = user.videoStatus.isOn
            }
            setPlaceholdersVisibility(selfVideoOn, otherVideoOn)
        }
    }

    private fun showAltPlaceHolder(show: Boolean) {
        with(binding) {
            if (show) {
                tvAltPlaceholder.toVisible()
                altBackground.toVisible()
            } else {
                tvAltPlaceholder.toGone()
                altBackground.toGone()
            }
        }
    }

    private fun subscribeUserVideo(view: ZoomVideoSDKVideoView, user: ZoomVideoSDKUser?) {
        user?.videoCanvas?.subscribe(view, ZoomVideoSDKVideoAspect.ZoomVideoSDKVideoAspect_PanAndScan)
    }

    private fun unsubscribeUserVideo(view: ZoomVideoSDKVideoView, user: ZoomVideoSDKUser?) {
        user?.videoCanvas?.unSubscribe(view)
    }

    private fun handleZoomError(exception: ZoomError?) {
        var error = exception ?: return
        when (exception.code) {
            2010 -> error = ZoomError.ZoomSessionNotStarted
        }
        showDialogError(error)
        viewModel.resetError()
    }

    private fun getZoom() = viewModel.getZoom()

    override fun onQuarterSelected(quarter: Quarter) {
        quizHandler.selectAnswer(quarter)
        Log.e(TAG, "onQuarterSelected: ${quarter.name}")
    }

    override fun onGestureSelected(gesture: Gesture, quarter: Quarter) {
        Log.e(TAG, "onGestureSelected: ${gesture.name}")
    }

    override fun onConfirmationGestureSelected(quarter: Quarter, gesture: Gesture) {
        handAnalyzer?.stop()
        quizHandler.confirmAnswer(quarter, gesture)
    }

    override fun onMultipleHandsDetected() {
        quizHandler.showMultipleHandsWarning()
    }

    override fun onDestroy() {
        super.onDestroy()
        handAnalyzer?.close()
    }

    override fun onQuestionConfirmed(question: Question) {
        viewModel.sendQuestion(question)
    }
}
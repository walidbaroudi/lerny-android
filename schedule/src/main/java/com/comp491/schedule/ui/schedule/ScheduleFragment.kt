package com.comp491.schedule.ui.schedule

import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import com.comp491.common.Constants
import com.comp491.common.managers.PermissionHandler
import com.comp491.common.ui.base.BaseFragment
import com.comp491.common.ui.dialog.DoubleButtonDoubleMessageDialog
import com.comp491.common.utils.getColor
import com.comp491.common.utils.showDialogError
import com.comp491.domain.model.error.AppError
import com.comp491.domain.model.error.LernyError
import com.comp491.domain.model.error.ZoomError
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.utils.sendToCrashlytics
import com.comp491.domain.utils.toGone
import com.comp491.domain.utils.toVisible
import com.comp491.schedule.R
import com.comp491.schedule.databinding.FragmentScheduleBinding
import com.comp491.schedule.ui.adapters.ScheduledSessionAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel
import us.zoom.sdk.ZoomVideoSDK
import us.zoom.sdk.ZoomVideoSDKErrors
import us.zoom.sdk.ZoomVideoSDKInitParams
import us.zoom.sdk.ZoomVideoSDKRawDataMemoryMode

class ScheduleFragment : BaseFragment<FragmentScheduleBinding>(), PermissionHandler {

    private val viewModel: ScheduleViewModel by viewModel()

    private lateinit var meetingsAdapter: ScheduledSessionAdapter
    private var permissionDialog: DoubleButtonDoubleMessageDialog? = null
    private var pendingJoinMeeting: Meeting? = null

    override fun setupView() {
        meetingsAdapter = ScheduledSessionAdapter(listOf(), viewModel.isUserStudent()) { joinMeeting(it) }

        binding.btnNewMeeting.apply { if (viewModel.isUserStudent()) toGone() else toVisible() }
        binding.viewMeetingsRefresh.apply {
            setColorSchemeColors(getColor(R.color.ler_purple))
            setOnRefreshListener {
                flowManager().getUserMeetings(false)
            }
        }

        binding.recyclerScheduled.apply {
            adapter = meetingsAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    override fun setupListeners() {
        binding.btnNewMeeting.setOnClickListener {
            navigate(R.id.scheduel_to_new_meeting)
        }
    }

    override fun subscribesUI() {
        flowManager().getUserMeetings()
    }

    override fun handleUserMeetings(meetings: List<Meeting>?) {
        binding.viewMeetingsRefresh.isRefreshing = false
        meetings ?: return
        binding.tvNoMeetings.apply { if (meetings.isEmpty()) toVisible() else toGone() }
        meetingsAdapter.updateMeetings(meetings)
    }

    private fun joinMeeting(meeting: Meeting) {
        if (permissionsManager().checkZoomPermissions().not()) {
            pendingJoinMeeting = meeting
            return
        }
        val zoomReady = requireBaseActivity().isZoomReady()
        if (zoomReady) {
            navigate(
                R.id.schedule_to_meeting,
                bundleOf(Constants.KEY_MEETING to meeting)
            )
        } else {
            initZoom { joinMeeting(meeting) }
        }
    }

    private fun initZoom(onInitAction: (() -> Unit)? = null) {
        val modeHeap = ZoomVideoSDKRawDataMemoryMode.ZoomVideoSDKRawDataMemoryModeHeap
        val params = ZoomVideoSDKInitParams().apply {
            domain = "https://zoom.us"
            logFilePrefix = "LernyLog"
            enableLog = true
            videoRawDataMemoryMode = modeHeap
            audioRawDataMemoryMode = modeHeap
        }

        val sdk = ZoomVideoSDK.getInstance()
        val initResult = sdk.initialize(requireContext(), params)
        if (initResult == ZoomVideoSDKErrors.Errors_Success) {
            requireBaseActivity().setZoomReady()
            onInitAction?.invoke()
        } else {
            ZoomError.ZoomGeneralError("error code: $initResult", initResult).sendToCrashlytics()
            showDialogError(LernyError.UnknownError)
        }
    }

    override fun preViewCreate() {
        super.preViewCreate()
        setToolBarTitle(getString(R.string.scr_schedule_title))
        overrideBackBehavior(
            { backToRoot() },
            ignoresDefaultBehavior = true
        )
    }

    override fun onResume() {
        super.onResume()
        permissionsManager().setPermissionHandler(this)
    }

    override fun handleZoomPermissions(granted: Boolean) {
        if (granted)
            pendingJoinMeeting?.let { joinMeeting(it) }
        else
            showPermissionsNeededDialog()
    }

    private fun showPermissionsNeededDialog() {
        if (permissionDialog == null)
            permissionDialog = DoubleButtonDoubleMessageDialog(
                "Permission Needed",
                "The required permissions are needed for joining meetings",
                "Grant permissions",
                "Cancel",
                { permissionsManager().checkZoomPermissions() },
                cancellable = false
            )
        permissionDialog?.show(childFragmentManager)
        // TODO: if dialog not null, make positive button go to settings
    }
}
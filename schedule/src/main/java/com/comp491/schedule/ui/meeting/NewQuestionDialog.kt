package com.comp491.schedule.ui.meeting

import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import com.comp491.common.R
import com.comp491.common.ui.base.BaseDialogFragment
import com.comp491.common.ui.components.LernyInputField
import com.comp491.common.ui.components.ToggleView
import com.comp491.common.utils.Validator
import com.comp491.domain.model.meeting.Question
import com.comp491.domain.model.meeting.QuestionType
import com.comp491.domain.utils.toGone
import com.comp491.domain.utils.toVisible
import com.comp491.schedule.databinding.DialogNewQuestionBinding

// Using in the ActivityExt
class NewQuestionDialog(private val questionListener: QuestionConfirmedListener) : BaseDialogFragment<DialogNewQuestionBinding>(),
    ToggleView.OnToggleChangeListener {

    lateinit var fields: List<LernyInputField>
    lateinit var answerFields: List<LernyInputField>

    override fun setupView() {
        dialog!!.window?.setBackgroundDrawableResource(R.drawable.bg_round_corner_dialog)
        with(binding) {
            answerFields = listOf(inputAnswer1, inputAnswer2, inputAnswer3, inputAnswer4)
            fields = listOf(inputAnswer1, inputAnswer2, inputAnswer3, inputAnswer4, inputQuestion, inputTime)
        }
    }

    override fun setupListeners() {
        binding.btnConfirm.setOnClickListener {
            clearErrors()
            validateInput()
        }
        binding.toggleQuestionType.setOnToggleListener(this)
    }

    private fun validateInput() {
        val errorFields = mutableListOf<LernyInputField>()
        fields.forEach {
            if (it.isVisible)
                if (Validator.isValidInput(it.getText()).not())
                    errorFields.add(it)
        }
        if (errorFields.isEmpty())
            confirmQuestion()
        else
            errorFields.forEach {
                it.showError()
            }
    }

    private fun confirmQuestion() {
        with(binding) {
            val type = QuestionType.forValue(binding.toggleQuestionType.getSelectedText()) ?: return
            val question = Question(
                type,
                inputQuestion.getText(),
                answers = listOf(
                    inputAnswer1.getText(),
                    inputAnswer2.getText(),
                    inputAnswer3.getText(),
                    inputAnswer4.getText()
                ),
                inputTime.getText().toInt()
            )
            questionListener.onQuestionConfirmed(question)
            dismiss()
        }

    }

    private fun clearErrors() = fields.forEach { it.clearError() }

    interface QuestionConfirmedListener {
        fun onQuestionConfirmed(question: Question)
    }

    override fun onFirstChecked() {
        showAnswersFields(true)
    }

    override fun onSecondChecked() {
        showAnswersFields(false)
    }

    private fun showAnswersFields(show: Boolean) {
        if (show)
            answerFields.forEach { it.toVisible() }
        else
            answerFields.forEach { it.toGone() }
    }

}

package com.comp491.schedule.ui.meeting

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.comp491.common.ui.base.BaseViewModel
import com.comp491.data.utils.UiModelState
import com.comp491.data.utils.data
import com.comp491.domain.model.api.ScoreCreate
import com.comp491.domain.model.error.ZoomError
import com.comp491.domain.model.meeting.*
import com.comp491.domain.repos.DataRepository
import com.comp491.schedule.model.*
import com.comp491.domain.model.meeting.MeetingInteractionType.*
import com.comp491.domain.utils.*
import com.comp491.schedule.utils.getInitials
import com.google.gson.JsonElement
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import us.zoom.sdk.*

class MeetingViewModel(dataRepository: DataRepository) : BaseViewModel(dataRepository), ZoomVideoSDKDelegate {


    var mySelf: ZoomVideoSDKUser? = null
    var meetingId: String? = null
    var meetingName: String? = null
    var meetingEndpoint: String? = null
    lateinit var meetingQuestions: MutableList<Question>

    private var totalAnswers = 0
    private var correctAnswers = 0


    private val _sessionJoined = MutableLiveData<Boolean?>(null)
    val sessionJoined: LiveData<Boolean?> get() = _sessionJoined

    private val _errorCode = MutableLiveData<ZoomError?>(null)
    val errorCode: LiveData<ZoomError?> get() = _errorCode

    private val _users = MutableLiveData<ZoomUsersEvent?>(null)
    val users: LiveData<ZoomUsersEvent?> get() = _users

    private val _mainUser = MutableLiveData<ZoomVideoSDKUser>(null)
    val mainUser: LiveData<ZoomVideoSDKUser> get() = _mainUser

    private val _token = MutableStateFlow<UiModelState<String>>(UiModelState.None)
    val token: StateFlow<UiModelState<String>> get() = _token

    // Quiz related
    var lastQuestion: Question? = null

    private val _receivedQuestion = MutableLiveData<Question>()
    val receivedQuestion: LiveData<Question> get() = _receivedQuestion

    private val _receivedAnswer = MutableLiveData<Answer>()
    val receivedAnswer: LiveData<Answer> get() = _receivedAnswer

    private val _receivedEvaluation = MutableLiveData<AnswerEvaluation>()
    val receivedEvaluation: LiveData<AnswerEvaluation> get() = _receivedEvaluation
    //

    fun isSelfAudioDisconnected() = mySelf?.audioStatus?.let { status ->
        status.audioType == ZoomVideoSDKAudioStatus.ZoomVideoSDKAudioType.ZoomVideoSDKAudioType_None
    } ?: true

    fun isSelfMuted() = mySelf?.audioStatus?.isMuted ?: true

    fun isSelfVideoOn() = mySelf?.videoStatus?.isOn ?: false

    fun resetError() {
        _errorCode.value = null
    }

    fun generateToken(topic: String) {
        val role = if (isUserStudent()) 0 else 1
        flowWrapper(dataRepository.generateToken(topic, role), _token)
    }

    fun getToken() = _token.value.data.toSafeString("")

    fun isMyself(user: ZoomVideoSDKUser?) = user == mySelf

    fun isMyselfMain() = isMyself(_mainUser.value)

    fun getOtherUser() = getZoom().session.remoteUsers.firstOrNull()

    fun switchMainUser() {
        if (isMyself(_mainUser.value)) {
            getOtherUser()?.let { _mainUser.value = it }
        } else {
            _mainUser.value = mySelf
        }
    }

    fun setMyselfMainUser() {
        if (isMyself(_mainUser.value).not()) {
            _mainUser.value = mySelf
        }
    }

    fun getPlaceHolderTexts(): Pair<String?, String?> {
        val other = getOtherUser()
        return if (isMyself(_mainUser.value))
            Pair(mySelf?.getInitials(), other?.getInitials())
        else
            Pair(getOtherUser()?.getInitials(), mySelf?.getInitials())
    }

    fun getZoom(): ZoomVideoSDK = ZoomVideoSDK.getInstance()

    // Zoom controls

    override fun onSessionJoin() {
        _sessionJoined.value = true
    }

    override fun onSessionLeave() {
        _sessionJoined.value = false
    }

    override fun onError(errorCode: Int) {
        _errorCode.value = ZoomError.ZoomGeneralError("code $errorCode", errorCode)
    }

    override fun onUserJoin(helper: ZoomVideoSDKUserHelper?, userList: MutableList<ZoomVideoSDKUser>?) {
        // todo: filter 'me' out?
        val users = filterNotMe(userList)
        _users.value = ZoomUsersEvent(users, UserEvent.USER_JOIN)
    }

    override fun onUserLeave(helper: ZoomVideoSDKUserHelper?, userList: MutableList<ZoomVideoSDKUser>?) {
        val users = filterNotMe(userList)
        _users.value = ZoomUsersEvent(users, UserEvent.USER_LEAVE)
    }

    override fun onUserVideoStatusChanged(helper: ZoomVideoSDKVideoHelper?, userList: MutableList<ZoomVideoSDKUser>?) {
        _users.value = ZoomUsersEvent(userList, UserEvent.VIDEO_CHANGE)
    }

    override fun onUserAudioStatusChanged(helper: ZoomVideoSDKAudioHelper?, userList: MutableList<ZoomVideoSDKUser>?) {
        _users.value = ZoomUsersEvent(userList, UserEvent.AUDIO_CHANGE)
    }

    override fun onUserShareStatusChanged(p0: ZoomVideoSDKShareHelper?, p1: ZoomVideoSDKUser?, p2: ZoomVideoSDKShareStatus?) {}

    override fun onLiveStreamStatusChanged(p0: ZoomVideoSDKLiveStreamHelper?, p1: ZoomVideoSDKLiveStreamStatus?) {}

    override fun onChatNewMessageNotify(helper: ZoomVideoSDKChatHelper?, message: ZoomVideoSDKChatMessage?) {
        message?.let {
            if (it.isSelfSend.not())
                processMessage(it.content)
        }
    }

    override fun onUserHostChanged(p0: ZoomVideoSDKUserHelper?, p1: ZoomVideoSDKUser?) {}

    override fun onUserManagerChanged(p0: ZoomVideoSDKUser?) {}

    override fun onUserNameChanged(p0: ZoomVideoSDKUser?) {}

    override fun onUserActiveAudioChanged(p0: ZoomVideoSDKAudioHelper?, p1: MutableList<ZoomVideoSDKUser>?) {}

    override fun onSessionNeedPassword(p0: ZoomVideoSDKPasswordHandler?) {
        Log.d("TAG", "onSessionNeedPassword: ")
    }

    override fun onSessionPasswordWrong(p0: ZoomVideoSDKPasswordHandler?) {}

    override fun onMixedAudioRawDataReceived(p0: ZoomVideoSDKAudioRawData?) {}

    override fun onOneWayAudioRawDataReceived(p0: ZoomVideoSDKAudioRawData?, p1: ZoomVideoSDKUser?) {}

    override fun onShareAudioRawDataReceived(p0: ZoomVideoSDKAudioRawData?) {}

    override fun onCommandReceived(p0: ZoomVideoSDKUser?, p1: String?) {}

    override fun onCommandChannelConnectResult(p0: Boolean) {}

    override fun onCloudRecordingStatus(p0: ZoomVideoSDKRecordingStatus?) {}

    override fun onHostAskUnmute() {}

    override fun onInviteByPhoneStatus(p0: ZoomVideoSDKPhoneStatus?, p1: ZoomVideoSDKPhoneFailedReason?) {}


    private fun filterNotMe(userList: MutableList<ZoomVideoSDKUser>?) = userList?.filter { it != mySelf }?.toMutableList()

    private fun processMessage(message: String) {
        val interactionMessage = message.jsonToObject<InteractionMessage>() ?: return
        val interaction = interactionMessage.interaction
        when (interaction.code) {
            INTERACTION_QUESTION.ordinal -> {
                interaction.payload.toParcelable<Question>()?.let {
                    _receivedQuestion.value = it
                }
            }
            INTERACTION_ANSWER.ordinal -> {
                interaction.payload.toParcelable<Answer>()?.let {
                    _receivedAnswer.value = it
                }
            }
            INTERACTION_EVAL.ordinal -> {
                interaction.payload.toParcelable<AnswerEvaluation>()?.let {
                    _receivedEvaluation.value = it
                }
            }
        }
    }

    fun sendQuestion(question: Question) {
        lastQuestion = question
        val jsonQuestion = question.toJsonElement()
        sendInteraction(0, jsonQuestion)
    }

    fun sendAnswer(answer: Answer) {
        val jsonAnswer = answer.toJsonElement()
        sendInteraction(1, jsonAnswer)
    }

    fun sendEvaluation(evaluation: AnswerEvaluation) {
        val jsonEvaluation = evaluation.toJsonElement()
        sendInteraction(2, jsonEvaluation)
    }

    private fun sendInteraction(code: Int, payload: JsonElement) {
        getOtherUser()?.let { other ->
            val receiver = Receiver(other.userName, other.userID)
            val interaction = MeetingInteraction(code, payload)
            val message = InteractionMessage(interaction, receiver)
            sendMessage(message.toJson(), other)
        }
    }

    private fun sendMessage(message: String, user: ZoomVideoSDKUser) {
        runOnMainThread {
            getZoom().chatHelper.sendChatToUser(user, message)
        }
    }

    fun submitAnswer(answer: Answer) {
        sendAnswer(answer)
        totalAnswers++
    }

    fun recordEvaluation(evaluation: AnswerEvaluation) {
        if (evaluation.isCorrect) correctAnswers++
    }

    fun submitMeetingScore() {
        if (totalAnswers == 0) return
        meetingId ?: return
        val scorePercentage = (correctAnswers.toFloat() / totalAnswers) * 100
        val scoreCreate = ScoreCreate(scorePercentage.toInt(), meetingId!!)
        flowWrapper(dataRepository.submitScore(scoreCreate))
    }

    fun confirmationGesturesForQuestionType(type: QuestionType) = when (type) {
        QuestionType.MC -> listOf(Gesture.THUMBS_UP)
        QuestionType.TF -> listOf(Gesture.THUMBS_UP, Gesture.THUMBS_DOWN)
    }
}
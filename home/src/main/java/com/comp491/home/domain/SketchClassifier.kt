package com.comp491.home.domain

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import com.comp491.domain.utils.indicesOfMaxes
import org.tensorflow.lite.DataType
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import java.nio.ByteBuffer
import java.nio.ByteOrder
import com.comp491.home.ml.SketchModel
import com.comp491.home.ui.adapters.Sketch


class SketchClassifier(private val context: Context) {


    private fun classify(bitmap: Bitmap): FloatArray {
        val model = SketchModel.newInstance(context)

        val inputFeature = TensorBuffer.createFixedSize(intArrayOf(1, 28, 28, 1), DataType.FLOAT32)
        val buffer = bitmapToByteBuffer(bitmap)

        inputFeature.loadBuffer(buffer)
        val outputs = model.process(inputFeature)
        val outputFeature = outputs.outputFeature0AsTensorBuffer

        return outputFeature.floatArray
    }

    private fun bitmapToByteBuffer(bitmap: Bitmap): ByteBuffer {
        val pixels = IntArray(SKETCH_HEIGHT * SKETCH_HEIGHT)
        bitmap.getPixels(pixels, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)
        return pixelsToBuffer(pixels)
    }

    private fun pixelsToBuffer(pixels: IntArray): ByteBuffer {
        val buffer = ByteBuffer.allocateDirect(4 * DIM_BATCH_SIZE * SKETCH_HEIGHT * SKETCH_WIDTH * DIM_PIXEL_SIZE)
        buffer.order(ByteOrder.nativeOrder())
        pixels.forEach { pixel ->
            buffer.putFloat(rgbToSingleChannel(pixel))
        }
        return buffer
    }

    private fun rgbToSingleChannel(pixel: Int): Float {
        val red = pixel shr 16 and 0xFF
        val green = pixel shr 8 and 0xFF
        val blue = pixel and 0xFF
        val average = (red + green + blue) / 3.0f
        val normalizedAverage = average / 255.0f
        return 1 - normalizedAverage // invert pixel color
    }

    fun check(drawing: Bitmap, currentSketch: Sketch): Boolean {
        val probabilities = classify(drawing)
        val top = probabilities.indicesOfMaxes(3).map { Sketch.forIndex(it) }
        Log.d("SKETCH", top.toString())
        return currentSketch in top
    }

    companion object {
        const val SKETCH_HEIGHT = 28
        const val SKETCH_WIDTH = 28
        const val DIM_BATCH_SIZE = 1
        const val DIM_PIXEL_SIZE = 1
    }
}
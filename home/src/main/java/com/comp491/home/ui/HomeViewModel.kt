package com.comp491.home.ui

import com.comp491.common.ui.base.BaseViewModel
import com.comp491.domain.model.general.Score
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.repos.DataRepository

class HomeViewModel(dataRepository: DataRepository) : BaseViewModel(dataRepository) {

    var meetings: List<Meeting>? = null

}
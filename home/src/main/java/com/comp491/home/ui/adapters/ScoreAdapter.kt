package com.comp491.home.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.comp491.domain.model.general.Score
import com.comp491.domain.model.session.Meeting
import com.comp491.home.databinding.LayoutHomeScoreItemBinding

class ScoreAdapter(private val scores: List<Score>, private val meetings: List<Meeting>, private val cellClickedCallback: () -> Unit) :
    RecyclerView.Adapter<ScoreViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScoreViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutHomeScoreItemBinding.inflate(inflater, parent, false)
        return ScoreViewHolder(binding, cellClickedCallback)
    }

    override fun onBindViewHolder(holder: ScoreViewHolder, position: Int) {
        val score = scores[position]
        val meeting = meetings.firstOrNull { it.getId() == score.meetingId } ?: return
        holder.bind(score, meeting)
    }

    override fun getItemCount() = scores.size


}

class ScoreViewHolder(private val binding: LayoutHomeScoreItemBinding, private val cellClickedCallback: () -> Unit) : RecyclerView.ViewHolder(binding.root) {

    fun bind(score: Score, meeting: Meeting) {
        with(binding) {
            tvSessionName.text = meeting.meetingTitle
            tvDay.text = meeting.time.getTimeRangeString()
            tvPercentage.text = score.getPercentageString()
            root.setOnClickListener { cellClickedCallback.invoke() }
        }
    }
}

package com.comp491.home.ui.sketch

import com.comp491.common.ui.base.BaseViewModel
import com.comp491.domain.repos.DataRepository
import com.comp491.home.ui.adapters.Sketch

class SketchViewModel(dataRepository: DataRepository) : BaseViewModel(dataRepository) {
    lateinit var currentSketch: Sketch
}
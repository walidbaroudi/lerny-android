package com.comp491.home.ui.sketch

import com.comp491.common.ui.base.BaseFragment
import com.comp491.common.utils.delayedTask
import com.comp491.domain.utils.runOnMainThread
import com.comp491.domain.utils.toGone
import com.comp491.domain.utils.toVisible
import com.comp491.domain.utils.vibratePhone
import com.comp491.home.databinding.FragmentSketchBinding
import com.comp491.home.domain.SketchClassifier
import com.comp491.home.ui.adapters.Sketch
import org.koin.androidx.viewmodel.ext.android.viewModel


class SketchFragment : BaseFragment<FragmentSketchBinding>() {

    private lateinit var sketchClassifier: SketchClassifier
    private val viewModel: SketchViewModel by viewModel()

    override fun setupView() {
        sketchClassifier = SketchClassifier(requireContext())
        setSketch()
    }

    private fun setSketch() {
        Sketch.random().let {
            viewModel.currentSketch = it
            binding.tvSketch.text = it.toName()
        }
        clearSketch()
    }

    override fun setupListeners() {
        binding.btnClear.setOnClickListener { binding.viewPaint.clear() }
        binding.btnClassify.setOnClickListener { classifyCurrentDrawing() }
        binding.btnSkip.setOnClickListener { setSketch() }
    }

    private fun classifyCurrentDrawing() {
        val drawing = binding.viewPaint.normalizedBitmap
        val match = sketchClassifier.check(drawing, viewModel.currentSketch)
        playAnimation(match)
        if (match) handleMatch()
    }

    private fun handleMatch() {
        requireContext().vibratePhone()
        setSketch()
    }

    private fun clearSketch() {
        binding.viewPaint.clear()
    }

    private fun playAnimation(correct: Boolean) {
        val anim = if (correct) "correct_anim.json" else "incorrect_anim.json"
        binding.animResult.apply {
            setAnimation(anim)
            toVisible()
            playAnimation()
            delayedTask(1500) { runOnMainThread { this.toGone() } }
        }
    }

    override fun subscribesUI() {
    }

}
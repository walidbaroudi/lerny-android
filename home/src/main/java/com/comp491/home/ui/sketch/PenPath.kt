package com.comp491.home.ui.sketch

import android.graphics.Path

class PenPath(var color: Int, var strokeWidth: Int, var path: Path)
package com.comp491.home.ui

import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.comp491.common.ui.base.BaseFragment
import com.comp491.domain.model.general.Score
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.utils.toGone
import com.comp491.domain.utils.toInvisible
import com.comp491.domain.utils.toVisible
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.comp491.home.R
import com.comp491.home.databinding.FragmentHomeBinding
import com.comp491.home.ui.adapters.ScoreAdapter
import com.comp491.home.ui.adapters.SessionsAdapter

class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    private val viewModel: HomeViewModel by viewModel()

    override fun setupView() {
        binding.groupScores.apply { if (viewModel.isUserStudent()) toVisible() else toGone() }
        viewModel.getUser()?.let { user ->
            binding.tvName.text = user.name
        } ?: showSnackBar("NO USER", isError = true)
    }

    override fun handleUserMeetings(meetings: List<Meeting>?) {
        with(binding) {
            val upcoming = meetings?.filter { it.isPast().not() }
            if (upcoming.isNullOrEmpty()) {
                recyclerMeetings.toInvisible()
                layoutNoMeeting.toVisible()
            } else {
                recyclerMeetings.toVisible()
                layoutNoMeeting.toGone()
                recyclerMeetings.apply {
                    adapter = SessionsAdapter(upcoming, viewModel.isUserStudent()) { goToSchedule() } // todo: pass future meetings only
                    layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
                }
            }
        }
        viewModel.meetings = meetings
        getScores()
    }

    override fun handleUserScores(scores: List<Score>?) {
        with(binding) {
            if (scores.isNullOrEmpty()) {
                recyclerScores.toInvisible()
                layoutNoScores.toVisible()
            } else {
                recyclerScores.toVisible()
                layoutNoScores.toGone()
                recyclerScores.apply {
                    viewModel.meetings?.let {
                        adapter = ScoreAdapter(scores, it) { goToProfile() }
                    }
                    layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
                }
            }
        }
    }

    override fun setupListeners() {
        binding.layoutNoMeeting.setOnClickListener {
            navigate(R.id.home_to_sketch)
        }
        binding.layoutDraw.setOnClickListener {
            navigate(R.id.home_to_sketch)
        }
    }

    override fun subscribesUI() {
        flowManager().getUserMeetings()
    }

    private fun getScores() {
        if (viewModel.isUserStudent())
            flowManager().getUserScores()
    }

    override fun preViewCreate() {
        super.preViewCreate()
        showToolbar(false, showBackButton = false)
        setToolBarTitle(getString(R.string.scr_home_title))
    }

}
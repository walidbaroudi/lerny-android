package com.comp491.home.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.comp491.domain.model.session.Meeting
import com.comp491.home.databinding.LayoutSessionItemBinding

class SessionsAdapter(private val meetings: List<Meeting>, private val isCurrentStudent: Boolean, private val cellClickCallback: () -> Unit) :
    RecyclerView.Adapter<SessionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SessionViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutSessionItemBinding.inflate(inflater, parent, false)
        return SessionViewHolder(binding, isCurrentStudent, cellClickCallback)
    }

    override fun onBindViewHolder(holder: SessionViewHolder, position: Int) {
        holder.bind(meetings[position])
    }

    override fun getItemCount() = meetings.size


}

class SessionViewHolder(private val binding: LayoutSessionItemBinding, private val isCurrentStudent: Boolean, private val cellClickCallback: () -> Unit) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(meeting: Meeting) {
        with(binding) {
            tvSessionName.text = meeting.meetingTitle
            tvUserName.text = if (isCurrentStudent) meeting.tutor.getFullName() else meeting.student.getFullName()
            tvTime.text = meeting.time.getTimeRangeString()
            root.setOnClickListener { cellClickCallback.invoke() }
        }
    }
}

package com.comp491.common.managers

interface PermissionManager {
    fun setPermissionHandler(handler: PermissionHandler)

    fun checkZoomPermissions(): Boolean
}

interface PermissionHandler {
    fun handleZoomPermissions(granted: Boolean)
}
package com.comp491.common.managers

import com.comp491.domain.model.general.Score
import com.comp491.domain.model.session.Meeting

interface FlowManager {
    fun registerHandler(handler: FlowHandler)
    fun unregisterHandler()

    fun getUserMeetings(cache: Boolean = true)
    fun getUserScores(cache: Boolean = true)
    fun invalidateCachedData()
}

interface FlowHandler {
    fun handleUserMeetings(meetings: List<Meeting>?)
    fun handleUserScores(scores: List<Score>?)
}
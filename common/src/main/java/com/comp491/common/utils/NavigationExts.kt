package com.comp491.common.utils

import androidx.annotation.IdRes
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import com.comp491.common.R

fun NavController.getNavOptions(@IdRes popUpTo: Int?, popUpToInclusive: Boolean = false): NavOptions {
    val options = NavOptions.Builder()
        .setEnterAnim(R.anim.nav_default_enter_anim)
        .setExitAnim(R.anim.nav_default_exit_anim)
        .setPopEnterAnim(R.anim.nav_default_pop_enter_anim)
        .setPopExitAnim(R.anim.nav_default_pop_exit_anim)
    popUpTo?.let { options.setPopUpTo(it, popUpToInclusive) }
    return options.build()
}
package com.comp491.common.utils

import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import androidx.viewbinding.ViewBinding
import com.comp491.common.R
import com.comp491.common.ui.base.BaseActivity
import com.comp491.common.ui.dialog.ErrorDialog
import com.comp491.domain.model.error.AppError
import com.comp491.domain.model.error.BackendError
import com.comp491.domain.model.error.LernyError
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.lang.reflect.ParameterizedType
import java.net.ConnectException
import java.net.UnknownHostException


fun FragmentActivity.showDefaultAlert(title: String, body: String, buttonAction: (() -> Unit)? = null) =
    MaterialAlertDialogBuilder(this).apply {
        setTitle(title)
        setMessage(body)
        setPositiveButton(R.string.general_ok) { _, _ -> buttonAction?.invoke() }
    }.create().apply {
        setOnShowListener { getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getColor(android.R.color.holo_blue_dark)) }
        show()
    }

fun FragmentActivity.showOptionAlert(
    title: String,
    body: String,
    positiveButtonTitle: String,
    positiveButtonAction: () -> Unit,
    negativeButtonTitle: String,
    negativeButtonAction: () -> Unit
) {
    val alert = MaterialAlertDialogBuilder(this)
        .setTitle(title)
        .setMessage(body)
        .setPositiveButton(positiveButtonTitle) { _, _ -> positiveButtonAction.invoke() }
        .setNegativeButton(negativeButtonTitle) { _, _ -> negativeButtonAction.invoke() }
        .create()

    alert.setOnShowListener {
        val buttonColor = getColor(R.color.ler_purple)
        alert.getButton(AlertDialog.BUTTON_POSITIVE)
            .setTextColor(buttonColor)
        alert.getButton(AlertDialog.BUTTON_NEGATIVE)
            .setTextColor(buttonColor)
    }
    alert.show()
}


fun BaseActivity<*>.showDialogError(exception: Exception = Exception(""), action: (() -> Unit)? = null) {
    if (exception is BackendError.NoError) return
    val body = getErrorMessage(exception)
    ErrorDialog(body, action).show(supportFragmentManager)
}

fun BaseActivity<*>.getErrorMessage(exception: Exception): String {
    return if (exception is LernyError)
        exception.safeMessage()
    else {
        Log.e("ERROR", "getErrorMessage: ", exception)
        LernyError.UnknownError.safeMessage()
    }
}

fun <VB : ViewBinding> FragmentActivity.createBindingInstance(
    inflater: LayoutInflater
): VB {

    val vbType = (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0]
    val vbClass = vbType as Class<VB>
    val method = vbClass.getMethod(
        "inflate",
        LayoutInflater::class.java
    )

    // Call VB.inflate(inflater, container, false) Java static method
    return method.invoke(null, inflater) as VB
}

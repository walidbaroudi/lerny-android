package com.comp491.common.utils

import com.comp491.domain.utils.runOnMainThread
import java.util.*
import kotlin.concurrent.schedule

fun delayedTask(delay: Int, executeOnMainThread: Boolean = false, task: () -> Unit) =
    Timer("Loading", false).schedule(delay.toLong()) {
        if (executeOnMainThread)
            runOnMainThread {
                task.invoke()
            }
        else
            task.invoke()
    }


package com.comp491.common.utils

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.comp491.common.R
import com.comp491.common.ui.base.BaseFragment
import com.comp491.common.ui.dialog.ErrorDialog
import com.comp491.domain.model.error.AppError
import com.comp491.domain.model.error.LernyError
import java.lang.reflect.ParameterizedType
import java.net.ConnectException
import java.net.UnknownHostException

//
//
//import android.content.DialogInterface
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.CompoundButton
//import android.widget.TextView
//import androidx.annotation.StringRes
//import androidx.fragment.app.Fragment
//import androidx.lifecycle.lifecycleScope
//import androidx.viewbinding.ViewBinding
//import kotlinx.coroutines.FlowPreview
//import java.lang.reflect.ParameterizedType
//import java.net.ConnectException
//import java.net.UnknownHostException
//
//fun BaseFragment<*>.click(view: View, callback: (View) -> Unit) {
//    view.clicks().throttleFirst(1500).onEach {
//        try {
//            callback.invoke(view)
//            Timber.tag("!!!").d("Button Clicks")
//        } catch (e: Exception) {
//            e.sendToCrashlytics()
//            showDialogError()
//        }
//    }.launchIn(lifecycleScope)
//}
//
//fun BaseDialogFragment<*>.click(view: View, callback: (View) -> Unit) {
//    view.clicks().throttleFirst(1500).onEach {
//        try {
//            callback.invoke(view)
//            Timber.tag("!!!").d("Button Clicks")
//        } catch (e: Exception) {
//            e.sendToCrashlytics()
//            showError()
//        }
//    }.launchIn(lifecycleScope)
//}
//
//fun BaseBottomSheetDialog<*>.click(view: View, callback: (View) -> Unit) {
//    view.clicks().throttleFirst(1500).onEach {
//        try {
//            callback.invoke(view)
//            Timber.tag("!!!").d("Button Clicks")
//        } catch (e: Exception) {
//            e.sendToCrashlytics()
//            showError()
//        }
//    }.launchIn(lifecycleScope)
//}
//
//@FlowPreview
//fun BaseFragment<*>.checkChanges(
//    view: CompoundButton,
//    callback: (Boolean) -> Unit,
//    error: (Exception) -> Unit = {}
//) = view.checkedChanges().drop(1).onEach {
//    try {
//        callback(it)
//    } catch (e: Exception) {
//        e.sendToCrashlytics()
//        error(e)
//    }
//}.launchIn(lifecycleScope)
//
//@FlowPreview
//fun BaseFragment<*>.focusChanges(
//    view: View,
//    callback: (Boolean) -> Unit,
//    error: (Exception) -> Unit = {}
//) = view.focusChanges().drop(1).onEach {
//    try {
//        callback(it)
//    } catch (e: Exception) {
//        e.sendToCrashlytics()
//        error(e)
//    }
//}.launchIn(lifecycleScope)
//
//@FlowPreview
//fun Fragment.textChanges(
//    textView: TextView,
//    debounce: Long = 500,
//    callback: (CharSequence) -> Unit,
//    error: (Exception) -> Unit = {}
//) =
//    textView.textChanges().debounce(debounce).onEach {
//        try {
//            callback(it)
//        } catch (e: Exception) {
//            e.sendToCrashlytics()
//            error(e)
//        }
//    }.launchIn(lifecycleScope)
//
//fun Fragment.imeEvent(
//    textView: TextView,
//    callback: (Int) -> Unit,
//    error: (Exception) -> Unit = {}
//) =
//    textView.editorActions().throttleFirst(1500).onEach {
//        try {
//            callback(it)
//        } catch (e: Exception) {
//            e.sendToCrashlytics()
//            error(e)
//        }
//    }.launchIn(lifecycleScope)
//
fun <VB : ViewBinding> Fragment.createBindingInstance(
    inflater: LayoutInflater,
    container: ViewGroup? = null
): VB {
    val vbType = (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0]
    val vbClass = vbType as Class<VB>
    val method = vbClass.getMethod(
        "inflate",
        LayoutInflater::class.java,
        ViewGroup::class.java,
        Boolean::class.java
    )

    // Call VB.inflate(inflater, container, false) Java static method
    return method.invoke(null, inflater, container, false) as VB
}
//typealias ActionMeta<T> = (t: T, meta: Meta?) -> Unit
typealias Action<T> = (t: T) -> Unit
typealias ErrorAction = (exception: LernyError) -> Unit

//
//fun BaseFragment<*>.setToolbarTitle(@StringRes res: Int) {
//    if (activity !is BaseActivity<*>) return
//    (activity as BaseActivity<*>).setToolbarTitle(res)
//}
//
//fun BaseFragment<*>.setToolbarTitle(title: CharSequence) {
//    if (activity !is BaseActivity<*>) return
//    (activity as BaseActivity<*>).setToolbarTitle(title)
//}
//
///**
// * Show title or logo
// */
//fun BaseFragment<*>.showToolbarTitleOrLogo(showTitle: Boolean) {
//    if (activity !is BaseActivity<*>) return
//    if (showTitle) (activity as BaseActivity<*>).showToolbarTitle()
//    else (activity as BaseActivity<*>).showToolbarLogo()
//}
//
fun BaseFragment<*>.showSnackbarError(exception: Exception = Exception(""), lengthLong: Boolean = true) {
    showSnackBar(getErrorMessage(exception), lengthLong, true)
}

//
fun BaseFragment<*>.showDialogError(exception: Exception = Exception(""), action: (() -> Unit)? = null) {
    requireBaseActivity().showDialogError(exception, action)
}

//
//fun BaseFragment<*>.showOptionDialog(
//    title: String,
//    body: String,
//    positiveButtonTitle: String,
//    positiveButtonAction: () -> Unit,
//    negativeButtonTitle: String,
//    negativeButtonAction: () -> Unit
//) {
//    activity?.showOptionAlert(title, body, positiveButtonTitle, positiveButtonAction, negativeButtonTitle, negativeButtonAction)
//}
//
fun BaseFragment<*>.getErrorMessage(exception: Exception): String {
    return if (exception is LernyError)
        exception.safeMessage()
    else {
        Log.e("ERROR", "getErrorMessage: ", exception)
        LernyError.UnknownError.safeMessage()
    }
}

fun BaseFragment<*>.getColor(@ColorRes colorId: Int) = ContextCompat.getColor(requireContext(), colorId)

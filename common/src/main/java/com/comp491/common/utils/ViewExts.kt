package com.comp491.common.utils

import android.animation.ValueAnimator
import android.content.res.ColorStateList
import android.view.View
import android.view.ViewTreeObserver
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import androidx.constraintlayout.widget.Group

fun View.animateScale(scale: Float, animDuration: Long = 200) {
    animate().scaleY(scale).scaleX(scale).setInterpolator(DecelerateInterpolator()).duration = animDuration
}

fun View.setScale(scale: Float) {
    scaleX = scale
    scaleY = scale
}

fun ImageView.animateTint(startColor: Int, endColor: Int, duration: Long = 1000) {
    animateColor(startColor, endColor, duration) {
        imageTintList = ColorStateList.valueOf(it)
    }
}

fun View.animateBackgroundTint(startColor: Int, endColor: Int, duration: Long = 1000) {
    animateColor(startColor, endColor, duration) {
        backgroundTintList = ColorStateList.valueOf(it)
    }
}

fun Group.animateBackgroundTint(startColor: Int, endColor: Int, duration: Long = 1000) {
    val root = this.rootView
    referencedIds.forEach {
        root.findViewById<View>(it).animateBackgroundTint(startColor, endColor, duration)
    }
}

private fun animateColor(startColor: Int, endColor: Int, duration: Long, onColor: (Int) -> Unit) {
    ValueAnimator.ofArgb(startColor, endColor).apply {
        this.duration = duration
        addUpdateListener { anim -> onColor.invoke(anim.animatedValue as Int) }
        interpolator = DecelerateInterpolator()
        start()
    }
}

fun View.handleGlobalLayout(callback: (() -> Unit)) {
    val observer: ViewTreeObserver = viewTreeObserver
    observer.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            viewTreeObserver.removeOnGlobalLayoutListener(this)
            callback.invoke()
        }
    })
}

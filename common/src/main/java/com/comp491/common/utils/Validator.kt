package com.comp491.common.utils

object Validator {
    fun isValidEmail(email: String): Boolean {
        val emailPattern = "\\w+@\\w+\\..{2,}".toRegex()
        return email.matches(emailPattern)
    }

    fun isValidPassword(password: String) = password.length > 3
    fun isMatchingPassword(password: String, confirm: String) = password == confirm
    fun isValidName(name: String?) = name.isNullOrBlank().not()
    fun isValidAge(name: String?) = name.isNullOrBlank().not()

    fun isValidInput(input: String?) = input.isNullOrBlank().not()
}
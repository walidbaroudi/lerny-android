package com.comp491.common.ui.dialog

import android.view.View
import androidx.fragment.app.FragmentManager
import com.comp491.common.R
import com.comp491.common.databinding.DialogNoConnectionBinding
import com.comp491.common.ui.base.BaseDialogFragment
import com.comp491.common.utils.delayedTask

// Using in the ActivityExt
class NoConnectionDialog(private val retryButton: View.OnClickListener? = null) : BaseDialogFragment<DialogNoConnectionBinding>() {

    private var bindingReady = false

    override fun setupView() {
        isCancelable = false
        bindingReady = true
    }

    override fun setupListeners() {
        binding.btnRetry.apply {
            // note: do not change to 'click'
            setOnClickListener {
                showProgress(true)
                delayedTask(500) { onRetryClicked(this) }
            }
        }
    }

    private fun onRetryClicked(view: View?) {
        retryButton?.onClick(view)
    }

    override fun show(manager: FragmentManager) {
        showProgress(false)
        if (isVisible) return
        super.show(manager, this::class.java.canonicalName)
    }

    fun safeDismiss() {
        showProgress(false)
        if (isVisible.not()) return
        dismiss()
    }

    private fun showProgress(show: Boolean) {
        if (bindingReady.not()) return
        binding.btnRetry.isEnabled = show.not() // todo: make progress button instead
    }
}

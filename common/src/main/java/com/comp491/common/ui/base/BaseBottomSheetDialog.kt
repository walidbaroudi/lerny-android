package com.comp491.common.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.FragmentManager
import androidx.viewbinding.ViewBinding
import com.comp491.common.R
import com.comp491.common.utils.createBindingInstance
import com.comp491.common.utils.handleGlobalLayout
import com.comp491.domain.utils.sendToCrashlytics
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

abstract class BaseBottomSheetDialog<VB : ViewBinding> : BottomSheetDialogFragment() {

    protected lateinit var binding: VB

    abstract fun setupView()
    abstract fun setupListeners()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.MyBottomSheetDialog)
    }

    override fun getTheme(): Int {
        return R.style.MyBottomSheetDialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = createBindingInstance(inflater, container)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        expendToFull(view)
        setupView()
        setupListeners()
    }

    protected fun isBindingReady(): Boolean {
        return this::binding.isInitialized
    }

    protected open fun needToExpandFull(): Boolean {
        return false
    }

    private fun expendToFull(view: View) {
        if (needToExpandFull())
            view.handleGlobalLayout {
                try {
                    val dialog = dialog as BottomSheetDialog?
                    val bottomSheet =
                        dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                    val behavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(bottomSheet)
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED)
                } catch (e: Exception) {
                    e.sendToCrashlytics()
                }
            }
    }

    /**
     * Shows the dialog
     * @param manager: the fragment manager for showing the dialog
     */
    open fun show(manager: FragmentManager) {
        if (this.isVisible.not())
            this.show(manager, this::class.java.canonicalName)
    }
}

package com.comp491.common.ui.dialog

import com.comp491.common.databinding.DialogDoubleButtonDoubleMessageBinding
import com.comp491.common.databinding.DialogSingleButtonDoubleMessageBinding
import com.comp491.common.ui.base.BaseBottomSheetDialog
import com.comp491.common.ui.base.BaseDialogFragment


class DoubleButtonDoubleMessageDialog(
    private val primaryMessage: CharSequence,
    private val secondaryMessage: CharSequence,
    private val positiveBtnText: String,
    private val negativeBtnText: String,
    private val positiveAction: () -> Unit = { },
    private val negativeAction: () -> Unit = { },
    private val cancellable: Boolean = true
) : BaseDialogFragment<DialogDoubleButtonDoubleMessageBinding>() {

    override fun setupView() {
        isCancelable = cancellable
        with(binding) {
            dialogMsgPrimary.text = primaryMessage
            dialogMsgSecondary.text = secondaryMessage
            dialogBtnPositive.text = positiveBtnText
            dialogBtnNegative.text = negativeBtnText
        }
    }

    override fun setupListeners() {
        with(binding) {
            dialogBtnPositive.setOnClickListener {
                positiveAction.invoke()
                dismiss()
            }
            dialogBtnNegative.setOnClickListener {
                negativeAction.invoke()
                dismiss()
            }
        }
    }

}
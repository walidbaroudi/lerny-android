package com.comp491.common.ui.components

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.children
import com.comp491.common.R
import com.comp491.common.databinding.LayoutToggleViewBinding
import com.comp491.domain.utils.toSafeString

class ToggleView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle) {


    private val binding = LayoutToggleViewBinding.inflate(LayoutInflater.from(context), this, true)
    private var toggleListener: OnToggleChangeListener? = null

    init {
        context.obtainStyledAttributes(attrs, R.styleable.ToggleView, defStyle, 0).let { attributes ->
            val firstButton = attributes.getString(R.styleable.ToggleView_firstButtonText).toSafeString("")
            val secondButton = attributes.getString(R.styleable.ToggleView_secondButtonText).toSafeString("")
            attributes.getBoolean(R.styleable.ToggleView_dark, false).takeIf { it }?.let { setDarkMode() }
            binding.btnFirst.text = firstButton
            binding.btnSecond.text = secondButton
            attributes.recycle()
        }
        setupToggleListener()
    }

    private fun setupToggleListener() {
        with(binding) {
            binding.group.addOnButtonCheckedListener { _, checkedId, isChecked ->
                if (isChecked) {
                    when (checkedId) {
                        btnFirst.id -> {
                            toggleListener?.onFirstChecked()
                        }
                        btnSecond.id -> {
                            toggleListener?.onSecondChecked()
                        }
                        else -> {}
                    }
                }
            }
        }
    }

    fun setOnToggleListener(listener: OnToggleChangeListener) {
        toggleListener = listener
    }

    private fun setDarkMode() {
        listOf(binding.btnFirst, binding.btnSecond).forEach { btn ->
            btn.apply {
                backgroundTintList = ContextCompat.getColorStateList(context, R.color.toggle_button_bg_tint)
                setTextColor(context.getColor(R.color.white))
            }
        }

    }

    fun getSelected() = if (binding.group.checkedButtonId == binding.btnFirst.id) 0 else 1
    fun getSelectedText(): String = with(binding) { if (group.checkedButtonId == btnFirst.id) btnFirst.text else btnSecond.text }.toString()

    interface OnToggleChangeListener {
        fun onFirstChecked()
        fun onSecondChecked()
    }
}
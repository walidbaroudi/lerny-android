package com.comp491.common.ui.dialog

import com.comp491.common.databinding.DialogSingleButtonDoubleMessageBinding
import com.comp491.common.ui.base.BaseBottomSheetDialog


class SingleButtonDoubleMessageDialog(
    private val primaryMessage: CharSequence,
    private val secondaryMessage: CharSequence,
    private val dialogBtnText: String,
    private val callback: () -> Unit = { }
) : BaseBottomSheetDialog<DialogSingleButtonDoubleMessageBinding>() {
    private var buttonCallback: (() -> Unit)? = null

    override fun setupView() {
        binding.dialogMsgPrimary.text = primaryMessage
        binding.dialogMsgSecondary.text = secondaryMessage
        binding.dialogBtnConfirm.text = dialogBtnText
        buttonCallback = callback
    }

    override fun setupListeners() {

        binding.dialogBtnConfirm.setOnClickListener {
            buttonCallback?.invoke()
            dismiss()
        }
    }

}
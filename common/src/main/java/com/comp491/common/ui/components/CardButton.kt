package com.comp491.common.ui.components

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.comp491.common.R
import com.comp491.common.databinding.LayoutCardButtonBinding
import com.comp491.domain.utils.toSafeString

class CardButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle) {

    private val binding =
        LayoutCardButtonBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        context.obtainStyledAttributes(attrs, R.styleable.CardButton, defStyle, 0).let { attributes ->
            val buttonText = attributes.getString(R.styleable.CardButton_text).toSafeString("")
            attributes.getResourceId(R.styleable.CardButton_icon, -1).takeIf { it != -1 }?.let {
                binding.imgIcon.setImageResource(it)
            }
            attributes.getBoolean(R.styleable.CardButton_light, false).let {
                if (it) setLightMode()
            }
            setText(buttonText)
            attributes.recycle()
        }
    }

    private fun setLightMode() {
        with(binding) {
            frame.setBackgroundResource(R.drawable.bg_light_gradient_rect)
            imgIcon.imageTintList = ColorStateList.valueOf(getColor(R.color.ler_purple))
            tvButtonText.setTextColor(getColor(R.color.ler_purple))
        }
    }

    private fun getColor(@ColorRes colorId: Int): Int {
        return ContextCompat.getColor(context, colorId)
    }

    override fun setOnClickListener(l: OnClickListener?) {
        binding.frame.setOnClickListener(l)
    }

    fun setText(text: String) {
        binding.tvButtonText.text = text
    }
}
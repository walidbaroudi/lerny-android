package com.comp491.common.ui.dialog

import android.os.Bundle
import com.comp491.common.R
import com.comp491.common.databinding.DialogSingleButtonDoubleMessageBottomBinding
import com.comp491.common.ui.base.BaseBottomSheetDialog


class SingleButtonDoubleMessageBottomDialog(
    private val primaryMessage: CharSequence,
    private val secondaryMessage: CharSequence,
    private val dialogBtnText: String,
    private val callback: () -> Unit = { }
) : BaseBottomSheetDialog<DialogSingleButtonDoubleMessageBottomBinding>() {
    private var buttonCallback: (() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.MyBottomSheetDialog)
    }

    override fun getTheme(): Int {
        return R.style.MyBottomSheetDialog
    }

    override fun setupView() {
        binding.dialogMsgPrimary.text = primaryMessage
        binding.dialogMsgSecondary.text = secondaryMessage
        binding.dialogBtnConfirm.text = dialogBtnText
        buttonCallback = callback
    }

    override fun setupListeners() {
        binding.btnClose.setOnClickListener { dismiss() }

        binding.dialogBtnConfirm.setOnClickListener {
            buttonCallback?.invoke()
            dismiss()
        }
    }

}
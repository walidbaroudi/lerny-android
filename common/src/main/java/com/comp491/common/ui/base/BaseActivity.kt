package com.comp491.common.ui.base

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.widget.TextView
import androidx.annotation.MenuRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.viewbinding.ViewBinding
import com.comp491.common.R
import com.comp491.common.managers.FlowManager
import com.comp491.common.managers.PermissionManager
import com.comp491.common.ui.dialog.NoConnectionDialog
import com.comp491.common.utils.*
import com.comp491.data.backend.RemoteConfig
import com.comp491.data.utils.UiModelState
import com.comp491.domain.utils.*
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


abstract class BaseActivity<VB : ViewBinding> : AppCompatActivity() {

    protected lateinit var binding: VB

    protected var backBehavior: (() -> Unit)? = null

    private var progressDialog: AlertDialog? = null

    protected var toolbar: Toolbar? = null
    protected var toolbarTitle: TextView? = null
    protected var toolbarBorder: View? = null

    private var isZoomInitialized = false

    var noConnectionDialog = NoConnectionDialog { checkInternetAvailable(this) }

    var networkListener: NetworkStatusListener? = null


    abstract fun setupView()

    abstract fun setupListeners()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RemoteConfig.init()
        binding = createBindingInstance(layoutInflater)
        setContentView(binding.root)
        setToolbar()
        setupView()
        setupListeners()
    }

    abstract fun setToolbar()

    open fun setupNavigation(
        navController: NavController,
        hideBackButtonList: List<Int> = listOf()
    ) {
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (hideBackButtonList.contains(destination.id)) {
                showNavigationIcon(false)
            } else if (appBarConfiguration.topLevelDestinations.contains(destination.id)) {
                showNavigationIcon(false)
            } else {
                showNavigationIcon(true)
            }
            toolbar?.setNavigationOnClickListener {
                onBackPressed()
            }
        }
    }

    fun showToolbar(lightStyle: Boolean, showBackButton: Boolean = true) {
        toolbar?.menu?.clear()
        showNavigationIcon(showBackButton)
        if (lightStyle) setToolbarLight() else setToolbarDark()
        supportActionBar?.apply {
            if (isShowing.not()) {
                show()
            }
        }
    }

    fun hideToolbar() {
        supportActionBar?.apply {
            if (isShowing) {
                toolbarBorder.toGone()
                hide()
            }
        }
    }

    private fun showNavigationIcon(show: Boolean) {
        if (show) toolbar?.setNavigationIcon(R.drawable.ic_arrow_back_light)
        else toolbar?.navigationIcon = null
    }

    fun showToolbarMenuOptions(@MenuRes menuID: Int, listener: Toolbar.OnMenuItemClickListener) {
        toolbar?.apply {
            inflateMenu(menuID)
            setOnMenuItemClickListener(listener)
        }
    }

    fun getToolbarMenu(): Menu? {
        return toolbar?.menu
    }

    fun setToolbarTitle(@StringRes res: Int) {
        toolbarTitle?.setText(res)
    }

    fun setToolbarTitle(title: CharSequence) {
        toolbarTitle?.text = title
    }

    fun showToolbarTitle(lightStyle: Boolean = false) {
        showToolbar(lightStyle)
        toolbarTitle.toVisible()
    }

    fun setToolbarDark() {
        toolbar?.let {
            it.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.ler_purple_dark))
            toolbarTitle?.setTextColor(ContextCompat.getColor(applicationContext, R.color.white))
            if (it.navigationIcon != null)
                it.setNavigationIcon(R.drawable.ic_arrow_back_light)
            toolbarBorder.toGone()
        }
    }

    fun setToolbarLight() {
        toolbar?.let {
            it.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.white))
            toolbarTitle?.setTextColor(ContextCompat.getColor(applicationContext, R.color.black))
            if (it.navigationIcon != null)
                it.setNavigationIcon(R.drawable.ic_arrow_back_dark)
            toolbarBorder.toVisible()
        }
    }

    private var networkCallBack = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            showNoConnectionDialog(true)
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            showNoConnectionDialog(false)
        }
    }

    private fun registerNetworkInfo() {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val builder = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        val networkRequest = builder.build()
        connectivityManager.registerNetworkCallback(networkRequest, networkCallBack)
        // Check initial state of Internet connection
        checkInternetAvailable(this)
    }

    private fun unregisterNetworkManager() {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        connectivityManager.unregisterNetworkCallback(networkCallBack)
    }

    private fun checkInternetAvailable(context: Context) {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkCapabilities = connectivityManager.activeNetwork ?: return showNoConnectionDialog(false)
        val actNw = connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return showNoConnectionDialog(false)
        when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> return showNoConnectionDialog(online = true, dismissIfOnline = true)
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> return showNoConnectionDialog(online = true, dismissIfOnline = true)
            else -> showNoConnectionDialog(false)
        }
    }

    private fun showNoConnectionDialog(online: Boolean, dismissIfOnline: Boolean = false) {
        runOnUiThread {
            if (online.not()) noConnectionDialog.show(supportFragmentManager)
            else if (online.and(dismissIfOnline)) {
                noConnectionDialog.safeDismiss()
                networkListener?.onNetworkAvailable()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        registerNetworkInfo()
    }

    override fun onPause() {
        unregisterNetworkManager()
        super.onPause()
    }

    fun overrideBackBehavior(behavior: () -> Unit, permanent: Boolean = false, ignoresDefaultBehavior: Boolean = false) {
        backBehavior = {
            behavior.invoke()
            if (ignoresDefaultBehavior.not())
                onBackPressed()
            if (permanent.not())
                backBehavior = null // set behavior back to default
        }
    }

    private fun isProgressShowing(): Boolean = progressDialog?.isShowing.isTrue()

    fun showProgress() {
        if (isProgressShowing().not()) {
            if (progressDialog == null) progressDialog = createProgressLoading(this)
            progressDialog?.show()
        }
    }

    open fun createProgressLoading(context: Context?): AlertDialog? {
        if (null == context) return null
        val builder = AlertDialog.Builder(context)
        val view: View = LayoutInflater.from(context).inflate(R.layout.progress_dialog, null)
        builder.setView(view)
        builder.setCancelable(false)
        val alertDialog = builder.create()
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()
        return alertDialog
    }

    fun hideProgress() = progressDialog?.dismiss()

    open fun showProgress(state: Boolean) {
        if (state)
            showProgress()
        else
            hideProgress()
    }

    fun showSuccess(action: () -> Unit) {
        val view: View = LayoutInflater.from(this).inflate(R.layout.success_dialog, null)
        AlertDialog.Builder(this).setView(view).setCancelable(false).create().let { dialog ->
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
            delayedTask(2500, true) {
                action.invoke()
                dialog.dismiss()
            }
        }
    }

    fun resetBackBehavior() {
        backBehavior = null
    }

    fun setNetworkStatusListener(networkStatusListener: NetworkStatusListener) {
        this.networkListener = networkStatusListener
    }

    abstract fun subscribeUI()

    fun <T> observeFlow(
        stateFlow: StateFlow<UiModelState<T>>,
        loading: Action<Unit>? = null,
        success: Action<T>? = null,
        error: ErrorAction? = null,
        end: Action<Unit>? = null,
        ignoreLoading: Boolean = false,
        ignoreError: Boolean = false,
        hideLoadingOnEnd: Boolean = true,
        cancelOnEnd: Boolean = false
    ) {
        lifecycleScope.launch {
            val hideLoading = hideLoadingOnEnd && !ignoreLoading
            stateFlow.collect { result ->
                when (result) {
                    is UiModelState.Loading -> {
                        loading?.invoke(Unit)
                        if (loading == null && !ignoreLoading) showProgress(true)
                    }
                    is UiModelState.Success<T> -> {
                        success?.invoke(result.data)
                        end?.invoke(Unit)
                        if (hideLoading) showProgress(false)
                        if (cancelOnEnd) cancel()
                    }

                    is UiModelState.Error -> {
                        val lernyError = result.exception
                        lernyError.let {
                            error?.invoke(lernyError)
                            end?.invoke(Unit)
                            if (error == null && !ignoreError) showDialogError()
                            showProgress(false)
                        }
                        if (cancelOnEnd) cancel()
                    }

                    is UiModelState.None -> {
                        if (hideLoading) showProgress(false)
                    }

                }
            }
        }
    }

    abstract fun permissionsManager(): PermissionManager

    abstract fun showBottomNavigation()
    abstract fun hideBottomNavigation()

    protected fun launch(action: () -> Unit) =
        lifecycleScope.launch { action.invoke() }

    abstract fun backToRoot(navController: NavController)
    abstract fun goToSchedule(navController: NavController)
    abstract fun goToProfile(navController: NavController)

    abstract fun getFlowManager(): FlowManager
    abstract fun logout()

    fun isZoomReady() = isZoomInitialized
    fun setZoomReady() {
        isZoomInitialized = true
    }
}

interface NetworkStatusListener {
    fun onNetworkAvailable()
    fun onNetworkLost()
}

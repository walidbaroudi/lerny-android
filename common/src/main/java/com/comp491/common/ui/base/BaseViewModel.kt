package com.comp491.common.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.comp491.data.utils.UiModelState
import com.comp491.data.utils.data
import com.comp491.data.utils.doOnError
import com.comp491.data.utils.succeeded
import com.comp491.domain.model.error.AppError
import com.comp491.domain.model.api.Response
import com.comp491.domain.model.error.LernyError
import com.comp491.domain.repos.DataRepository
import com.comp491.domain.utils.sendToCrashlytics
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

abstract class BaseViewModel(protected val dataRepository: DataRepository) : ViewModel() {

    protected fun <T> flowWrapper(
        input: Flow<Response<T>>,
        mutableStateFlow: MutableStateFlow<UiModelState<T>>,
        cache: Boolean = false
    ) {
        if ((mutableStateFlow.value.succeeded && cache).not())
            viewModelScope.launch {
                mutableStateFlow.value = UiModelState.Loading
                input.doOnError {
                    mutableStateFlow.value = UiModelState.Error(LernyError.OtherError(it.message, it))
                }.collect { result ->
                    if (result.data != null) {
                        mutableStateFlow.value = UiModelState.Success(result.data!!)
                    } else {
                        val error = result.getError()
                        mutableStateFlow.value = UiModelState.Error(error) // Reset error to avoid showing again when back
                    }
                }
            }
        else {
            viewModelScope.launch {
                mutableStateFlow.value.data?.let { data ->
                    input.collect {
                        mutableStateFlow.value = UiModelState.None
                        mutableStateFlow.value = UiModelState.Success(data).copy()
                    }
                }
            }
        }

    }

    protected fun <T> flowWrapper(
        input: Flow<Response<T>>,
    ) {
        viewModelScope.launch {
            input.doOnError {
                LernyError.OtherError(it.message, it).sendToCrashlytics()
            }.collect { result ->
                if (result.data == null) {
                    val error = result.getError()
                    error.sendToCrashlytics()
                }
            }
        }
    }

    fun getUser() = dataRepository.getUser()
    fun isUserStudent() = dataRepository.getUser()?.isStudent() ?: false
    fun isUserTutor() = dataRepository.getUser()?.isTutor() ?: false
}
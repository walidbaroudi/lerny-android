package com.comp491.common.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.annotation.MenuRes
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.comp491.common.R
import com.comp491.common.managers.FlowHandler
import com.comp491.common.utils.*
import com.comp491.data.utils.UiModelState
import com.comp491.domain.model.general.Score
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.utils.sendToCrashlytics
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

abstract class BaseFragment<VB : ViewBinding> : Fragment(), NetworkStatusListener, FlowHandler {

    protected lateinit var binding: VB
    private var _binding: VB? = null

    private val navController: NavController by lazy { findNavController() }

    private var savedInstanceState: Bundle? = null
    protected val TAG: String get() = javaClass.simpleName


    open fun handleArgs() {}
    abstract fun setupView()
    abstract fun setupListeners()
    abstract fun subscribesUI() // used to observe ViewModels UiState members

    @CallSuper
    open fun preViewCreate() {
        showBottomNavigation()
        showToolbar(false)
        resetBackBehavior()
    } // used to setup options prior to displaying views

    open var screenNumber: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (_binding == null) {
            _binding = createBindingInstance(inflater, container)
            binding = _binding!!
        }

        preViewCreate()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        handleArgs()
        setupView()
        setupListeners()
        subscribesUI()
    }

    override fun onResume() {
        super.onResume()
        setNetworkStatusListener()
        flowManager().registerHandler(this)
    }

    override fun onStop() {
        super.onStop()
        flowManager().unregisterHandler()
    }

    fun getSavedInstanceState() = savedInstanceState

    open fun showCustomAlert(
        title: String,
        body: String,
        textP: String,
        callbackP: () -> Unit,
        textN: String? = null,
        callbackN: () -> Unit = {}
    ) {
        activity?.let {
            val dialog = MaterialAlertDialogBuilder(it)
                .setTitle(title)
                .setMessage(body)
                .setPositiveButton(
                    textP
                ) { _, _ -> callbackP() }

            if (textN != null) {
                dialog.setNegativeButton(textN) { _, _ -> callbackN() }
            }
            dialog.create().show()
        }
    }


    fun onBackPressed(callback: () -> Unit) {
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            callback()
        }
    }

    fun <T> observeFlow(
        stateFlow: StateFlow<UiModelState<T>>,
        loading: Action<Unit>? = null,
        success: Action<T>? = null,
        error: ErrorAction? = null,
        end: Action<Unit>? = null,
        ignore_loading: Boolean = false,
        ignore_error: Boolean = false,
        showSuccess: Boolean = false
    ) {
        viewLifecycleOwner.lifecycleScope.launch {
            stateFlow.collect { result ->
                when (result) {
                    is UiModelState.Loading -> {
                        loading?.invoke(Unit)
                        if (loading == null && !ignore_loading) {
                            showProgress(true)
                        }
                    }
                    is UiModelState.Success<T> -> {
                        showProgress(false)
                        if (showSuccess) showSuccess {
                            success?.invoke(result.data)
                            end?.invoke(Unit)
                        } else {
                            success?.invoke(result.data)
                            end?.invoke(Unit)
                        }
                    }
                    is UiModelState.Error -> {
                        val exception = result.exception
                        exception.sendToCrashlytics()
                        error?.invoke(exception)
                        end?.invoke(Unit)
                        if (error == null && !ignore_error) {
                            showDialogError(exception)
                        }
                        showProgress(false)
                    }

                    is UiModelState.None -> {
                        showProgress(false)
                    }
                }
            }
        }
    }

    protected fun navigateBack() = requireActivity().runOnUiThread { navController.navigateUp() }


    fun showToolbar(lightStyle: Boolean, showBackButton: Boolean = true) = requireBaseActivity().showToolbar(lightStyle, showBackButton)
    fun hideToolbar() = requireBaseActivity().hideToolbar()
    fun setToolBarTitle(title: String) = requireBaseActivity().setToolbarTitle(title)
    fun showToolBarTitle() = requireBaseActivity().showToolbarTitle()


    fun showToolbarMenuOptions(@MenuRes menuID: Int, listener: Toolbar.OnMenuItemClickListener) {
        requireBaseActivity().showToolbarMenuOptions(menuID, listener)
    }

    fun setToolbarDark() = requireBaseActivity().setToolbarDark()
    fun setToolbarLight() = requireBaseActivity().setToolbarLight()
    fun getToolbarMenu() = requireBaseActivity().getToolbarMenu()
    fun showBottomNavigation() = requireBaseActivity().showBottomNavigation()
    fun hideBottomNavigation() = requireBaseActivity().hideBottomNavigation()

    fun showSnackBar(message: String, lengthLong: Boolean = true, isError: Boolean = false) {
        try {
            binding.root.let {
                Snackbar.make(
                    it, message,
                    if (lengthLong) Snackbar.LENGTH_LONG else Snackbar.LENGTH_SHORT
                ).setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                    .setBackgroundTint(ContextCompat.getColor(requireContext(), if (isError) R.color.ler_red else R.color.ler_purple))
                    .show()
            }
        } catch (e: Exception) {
            e.sendToCrashlytics()
        }
    }

    fun showIndefiniteSnackBar(message: String) {
        try {
            binding.root.let {
                Snackbar.make(it, message, Snackbar.LENGTH_INDEFINITE)
            }.apply {
                setAction("OK") { dismiss() }
                show()
            }
        } catch (e: Exception) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    fun showToast(message: String, lengthLong: Boolean = false) {
        Toast.makeText(
            requireContext(),
            message,
            if (lengthLong) Toast.LENGTH_LONG else Toast.LENGTH_SHORT
        ).show()
    }

    fun overrideBackBehavior(behavior: () -> Unit, permanent: Boolean = false, ignoresDefaultBehavior: Boolean = false) {
        requireBaseActivity().overrideBackBehavior(behavior, permanent, ignoresDefaultBehavior)
    }

    fun resetBackBehavior() = requireBaseActivity().resetBackBehavior()

    fun requirePackageName(): String = requireActivity().applicationContext.packageName

    fun showProgress(state: Boolean) = requireBaseActivity().showProgress(state)

    fun showSuccess(successAction: () -> Unit) = requireBaseActivity().showSuccess(successAction)

    fun onBackPressed() = requireBaseActivity().onBackPressed()

    private fun setNetworkStatusListener() = requireBaseActivity().setNetworkStatusListener(this)


    override fun onNetworkAvailable() {}
    override fun onNetworkLost() {}

    fun requireBaseActivity(): BaseActivity<*> = (requireActivity() as BaseActivity<*>)

    protected fun launch(action: () -> Unit) =
        lifecycleScope.launch { action.invoke() }

    protected fun backToRoot() = requireBaseActivity().backToRoot(findNavController())
    protected fun goToSchedule() = requireBaseActivity().goToSchedule(findNavController())
    protected fun goToProfile() = requireBaseActivity().goToProfile(findNavController())

    protected fun navigate(@IdRes actionID: Int, args: Bundle? = null) {
        navController.navigate(actionID, args)
    }

    protected fun permissionsManager() = requireBaseActivity().permissionsManager()

    protected fun flowManager() = requireBaseActivity().getFlowManager()

    // Flow handler implementations
    override fun handleUserMeetings(meetings: List<Meeting>?) {}
    override fun handleUserScores(scores: List<Score>?) {}


    fun logout() {
        requireBaseActivity().logout()
    }
}


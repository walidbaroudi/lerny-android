package com.comp491.common.ui.dialog

import android.view.View
import androidx.fragment.app.FragmentManager
import com.comp491.common.R
import com.comp491.common.databinding.DialogErrorBinding
import com.comp491.common.ui.base.BaseDialogFragment

// Using in the ActivityExt
class ErrorDialog(private val message: String, private val action: (() -> Unit)? = null) : BaseDialogFragment<DialogErrorBinding>() {

    override fun setupView() {
        binding.lblDescription.text = message
    }

    override fun setupListeners() {
        binding.btnOkay.setOnClickListener {
            action?.invoke()
            safeDismiss()
        }
    }

    override fun show(manager: FragmentManager) {
        if (isVisible) return
        super.show(manager, this::class.java.canonicalName)
    }

    private fun safeDismiss() {
        if (isVisible.not()) return
        dismiss()
    }

}

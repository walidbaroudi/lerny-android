package com.comp491.common.ui.components

import android.content.Context
import android.content.res.ColorStateList
import android.text.InputType
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.comp491.common.R
import com.comp491.common.databinding.LayoutLernyInputFieldBinding
import com.comp491.domain.utils.toGone
import com.comp491.domain.utils.toVisible

class LernyInputField @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle) {

    private val binding = LayoutLernyInputFieldBinding.inflate(LayoutInflater.from(context), this, true)

    private val errorColor = ContextCompat.getColor(context, R.color.ler_red)
    private val errorBg = ContextCompat.getDrawable(context, R.drawable.error_textfield_round_bg)

    private val normalColor = ContextCompat.getColor(context, R.color.ler_gray)
    private val normalBg = ContextCompat.getDrawable(context, R.drawable.textfield_round_bg)


    init {
        with(binding) {
            context.obtainStyledAttributes(attrs, R.styleable.LernyInputField, defStyle, 0).let { attrs ->
                // set hint
                binding.editRoot.hint = attrs.getString(R.styleable.LernyInputField_inputHint)
                // set type
                val type = attrs.getInt(R.styleable.LernyInputField_inputType, 0)
                setInputType(type)
                // set error message
                val errorMessage = attrs.getString(R.styleable.LernyInputField_errorMessage)
                tvError.text = errorMessage

                attrs.recycle()
            }
        }
    }

    private fun setInputType(type: Int) {
        binding.editField.inputType = when (type) {
            1 -> {
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            }
            2 -> {
                InputType.TYPE_CLASS_NUMBER
            }
            3 -> {
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            }
            4 -> {
                binding.editRoot.setBackgroundResource(R.drawable.multiline_textfield_round_bg)
                binding.editField.apply {
                    gravity = Gravity.TOP
                    setLines(5)
                }
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE
            }
            else -> {
                InputType.TYPE_CLASS_TEXT
            }
        }
    }

    fun showError() {
        setErrorState(true)
    }

    fun clearError() {
        setErrorState(false)
    }

    fun getText() = binding.editField.text.toString()
    fun setText(text: String) {
        binding.editField.setText(text)
    }


    private fun setErrorState(isError: Boolean) {
        val color = if (isError) errorColor else normalColor
        val bg = if (isError) errorBg else normalBg
        binding.editRoot.apply {
            background = bg
            hintTextColor = ColorStateList.valueOf(color)
            defaultHintTextColor = ColorStateList.valueOf(color)
        }
        binding.tvError.apply {
            if (isError) {
                if (text.isNotBlank()) toVisible()
            } else toGone()
        }
    }

    override fun setOnClickListener(l: OnClickListener?) {
        binding.editField.apply {
            isFocusable = false
            isCursorVisible = false
            setOnClickListener(l)
        }
    }
}
package com.comp491.common

object Constants {

    const val LERNY_URL = "https://lerny.school"

    // KEYS
    const val KEY_USER_TYPE = "USER_TYPE"
    // Meeting
    const val KEY_MEETING= "KEY_MEETING"

    const val PERMISSION_ZOOM = 2525
}
package com.comp491.domain.utils

import com.comp491.domain.model.general.SimpleDate
import com.comp491.domain.model.general.SimpleTime
import java.util.*
import kotlin.math.absoluteValue

const val ONE_SECOND = 1000L
const val ONE_MINUTE = ONE_SECOND * 60L
const val ONE_HOUR = ONE_MINUTE * 60L
const val ONE_DAY = ONE_HOUR * 24L
const val ONE_MONTH = ONE_DAY * 30L


fun getTimeMillis(year: Int, month: Int, day: Int, hour: Int, minute: Int) =
    GregorianCalendar(year, month, day, hour, minute).apply { timeZone = TimeZone.getDefault() }.timeInMillis

fun getTimeMillis(date: SimpleDate, time: SimpleTime) =
    GregorianCalendar(date.year, date.month, date.day, time.hour, time.minute).apply { timeZone = TimeZone.getDefault() }.timeInMillis

fun getHour(millis: Long) = getDate(millis).get(Calendar.HOUR_OF_DAY)
fun getMinute(millis: Long) = getDate(millis).get(Calendar.MINUTE)
fun getDayOfMonth(millis: Long) = getDate(millis).get(Calendar.DAY_OF_MONTH)
fun getDayOfWeek(millis: Long) = getDate(millis).get(Calendar.DAY_OF_WEEK)
fun getDayOfWeekName(millis: Long) = getDayName(millis)
fun getMonthIndex(millis: Long) = getDate(millis).apply { time = Date(millis) }.get(Calendar.MONTH)
fun getMonth(millis: Long) = getMonthIndex(millis) + 1
fun getYear(millis: Long) = getDate(millis).get(Calendar.YEAR)

fun getDateString(millis: Long): String {
    val day = getDayOfMonth(millis)
    val month = getMonth(millis)
    val year = getYear(millis)
    return "${day.asMinTwoDigitString()}/${month.asMinTwoDigitString()}/$year"
}

/**
 * returns new date 'hours' hours after 'dateMillis
 */
fun plusHour(dateMillis: Long, hours: Int) = getDate(dateMillis).apply { add(Calendar.HOUR_OF_DAY, hours) }.timeInMillis

/**
 * returns new date 'minutes' minutes after 'dateMillis
 */
fun datePlusMinute(dateMillis: Long, minutes: Int) = getDate(dateMillis).apply { add(Calendar.MINUTE, minutes) }.timeInMillis
fun datePlusDay(dateMillis: Long, days: Int) = getDate(dateMillis).apply { add(Calendar.DAY_OF_YEAR, days) }.timeInMillis

private fun getDate(millis: Long) = GregorianCalendar(TimeZone.getDefault()).apply { time = Date(millis) }

fun now() = Date().time
fun millisToMinutes(millis: Long) = millis / 1000 / 60
fun millisToHours(millis: Long) = millisToMinutes(millis) / 60
fun millisToDays(millis: Long) = millisToHours(millis) / 24

private fun getDayName(dayNumber: Int): String = when (dayNumber) {
    1 -> "Sunday"
    2 -> "Monday"
    3 -> "Tuesday"
    4 -> "Wednesday"
    5 -> "Thursday"
    6 -> "Friday"
    7 -> "Saturday"
    else -> "??"
}

private fun getDayName(dateMillis: Long): String = getDayName(getDayOfWeek(dateMillis))

fun isOnSameDay(first: Long, second: Long): Boolean {
    val beginOfFirst = plusHour(first, -getHour(first))
    return (second - beginOfFirst) in 0..ONE_DAY
}
package com.comp491.domain.utils

import java.util.Collections.min

fun getStackTrace(): List<String> {
    val trace = Throwable().stackTrace
    val until = min(listOf(11, trace.size))
    val shortenedTrace = trace.slice(1..until)
    return shortenedTrace.map { with(it) { "[$fileName:$lineNumber] $methodName" } }
}

fun getStackTraceString() = getStackTrace().drop(1).joinToString(" <-- ")
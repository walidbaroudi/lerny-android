package com.comp491.domain.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

fun runAsync(action: suspend () -> Unit) {
    CoroutineScope(Dispatchers.Default).launch { action.invoke() }
}
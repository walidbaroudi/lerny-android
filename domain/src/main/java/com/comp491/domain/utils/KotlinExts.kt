package com.comp491.domain.utils

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Parcelable
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.ColorInt
import androidx.constraintlayout.widget.Group
import androidx.core.view.allViews
import com.comp491.domain.model.error.LernyError
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.internal.LinkedTreeMap
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

fun View?.toGone() {
    if (this == null) return
    if (visibility == View.GONE) return
    visibility = View.GONE
}

fun View?.toVisible() {
    if (this == null) return
    if (visibility == View.VISIBLE) return
    visibility = View.VISIBLE
}

fun Group?.groupToVisible(root: View) {
    this?.referencedIds?.forEach {
        root.findViewById<View>(it).toVisible()
    }
}

fun Group?.groupToGone(root: View) {
    this?.referencedIds?.forEach {
        root.findViewById<View>(it).toGone()
    }
}

fun Group.setBackgroundTintColor(@ColorInt color: Int) {
    val root = rootView
    referencedIds.forEach {
        root.findViewById<View>(it).backgroundTintList = ColorStateList.valueOf(color)
    }
}

fun View?.toInvisible() {
    if (this == null) return
    if (visibility == View.INVISIBLE) return
    visibility = View.INVISIBLE
}

fun View.showSoftKeyboard() {
    this.requestFocus()
    val inputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun Int.toBoolean() = this != 0

// Other

fun Exception.sendToCrashlytics() {
    Firebase.crashlytics.recordException(this)
    Firebase.crashlytics.log("[message] ${message ?: "No error message!"}")
    if (this is LernyError)
        Firebase.crashlytics.log("[trace] ${trace ?: "No trace!"}")
}

fun Any.toJson(): String = Gson().toJson(this)
fun Any.toJsonElement(): JsonElement = Gson().toJsonTree(this)

inline fun <reified T> String.jsonToObject(): T? {
    return try {
        Gson().fromJson(this, T::class.java)
    } catch (exc: Exception) {
        null
    }
}

inline fun <reified T> JsonElement.toParcelable(): T? {
    return try {
        Gson().fromJson(this, T::class.java)
    } catch (exc: Exception) {
        null
    }
}

fun Any?.toSafeString(altText: String = "Unknown", checkBlank: Boolean = false): String {
    return this?.toString().takeIf { if (checkBlank) it.isNullOrBlank().not() else it.notNull() } ?: altText
}

fun Any?.notNull() = isNull().not()

fun Any?.isNull() = this == null

fun Boolean?.isTrue() = this == true
fun Boolean?.notTrue() = this != true

fun FloatArray.indexOfMax() = maxOrNull()?.let { max -> indexOfFirst { it == max } }

fun FloatArray.indicesOfMaxes(num: Int): List<Int> {
    val cpy = this.copyOf()
    val maxes = mutableListOf<Int>()
    for (i in 0 until num) {
        val max = cpy.indexOfMax() ?: break
        maxes.add(max)
        cpy[max] = -1f
    }
    return maxes
}

fun (() -> Unit).post(scope: CoroutineScope, condition: () -> Boolean) {
    scope.launch {
        while (condition().not()) {
            delay(50)
        }
        invoke()
    }
}

fun Int.asMinTwoDigitString() = if (this > 9)
    "$this"
else
    "0$this"
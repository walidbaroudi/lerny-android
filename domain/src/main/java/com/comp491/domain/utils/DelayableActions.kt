package com.comp491.domain.utils

import android.os.CountDownTimer
import kotlinx.coroutines.CoroutineScope

class DelayableAction(msDelay: Long, private val endAction: () -> Unit, private val tickAction: ((Long) -> Unit)?) : CountDownTimer(msDelay, 1000) {

    var scope: CoroutineScope? = null
    var condition: (() -> Boolean)? = null
    private var isDelayStarted = false

    constructor(msDelay: Long, scope: CoroutineScope, action: () -> Unit, condition: (() -> Boolean)) : this(msDelay, action, null) {
        this.scope = scope
        this.condition = condition
    }

    constructor(msDelay: Long, action: () -> Unit) : this(msDelay, action, null)

    override fun onTick(millisUntilFinished: Long) {
        tickAction?.invoke(millisUntilFinished)
    }

    override fun onFinish() {
        if (condition.notNull().and(scope.notNull()))
            endAction.post(scope!!, condition!!)
        else
            endAction.invoke()
        isDelayStarted = false
    }

    fun delay() {
        this.cancel()
        this.start()
        isDelayStarted = true
    }

    fun invoke() {
        isDelayStarted = true
        start()
    }

    fun stop() {
        isDelayStarted = false
        cancel()
    }

    fun isStarted() = isDelayStarted
}
package com.comp491.domain.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

fun runOnMainThread(action: () -> Unit) {
    CoroutineScope(Dispatchers.Main).launch {
        action.invoke()
    }
}
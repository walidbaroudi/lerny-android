package com.comp491.domain.utils

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.os.VibratorManager

fun Context.checkPermissions(permissionKeys: List<String>): Boolean {
    for (key in permissionKeys)
        if (checkSelfPermission(key) != PackageManager.PERMISSION_GRANTED)
            return false
    return true
}

fun Context.vibratePhone() {
    val vibrator: Vibrator? =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            (getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager?)!!.defaultVibrator
        } else {
            @Suppress("DEPRECATION")
            getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?
        }

    val duration = 200L

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        vibrator!!.vibrate(VibrationEffect.createOneShot(duration, VibrationEffect.DEFAULT_AMPLITUDE))
    } else {
        @Suppress("DEPRECATION")
        vibrator!!.vibrate(duration)
    }
}
package com.comp491.domain.repos

import com.comp491.domain.model.api.*
import com.comp491.domain.model.general.Score
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.model.user.User
import kotlinx.coroutines.flow.Flow


abstract class DataRepository {

    // General
    abstract fun clearLocalData()

    // User
    abstract fun registerUser(register: UserRegister): Flow<Response<User>>
    abstract fun loginStudent(email: String, password: String): Flow<Response<User>>
    abstract fun setUser(user: User?)
    abstract fun getUser(): User?
    abstract fun isLoggedIn(): Boolean
    abstract fun getStudentScores(): List<Score>

    // Meetings
    abstract fun getUserMeetings(): Flow<Response<List<Meeting>>>
    abstract fun createMeeting(meeting: MeetingCreate): Flow<Response<Meeting>>
    abstract fun getUserScores(): Flow<Response<List<Score>>>
    abstract fun submitScore(score: ScoreCreate): Flow<Response<BlankData>>

    // Zoom
    abstract fun generateToken(topic: String, role: Int): Flow<Response<String>>

}

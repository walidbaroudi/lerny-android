package com.comp491.domain.model.general

import android.os.Parcelable
import com.comp491.domain.model.user.User
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserMeeting(
    val user: User,
    val meetingName: String
) : Parcelable

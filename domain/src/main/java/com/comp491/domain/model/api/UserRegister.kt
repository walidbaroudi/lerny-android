package com.comp491.domain.model.api

import com.google.gson.annotations.SerializedName

class UserRegister(
    val email: String,
    val password: String,
    val name: String,
    val surname: String,
    val gender: String,
    @SerializedName("entity") val type: String
)
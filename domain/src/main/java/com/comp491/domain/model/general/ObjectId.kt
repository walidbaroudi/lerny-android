package com.comp491.domain.model.general

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ObjectId(
    @SerializedName("\$oid") val id: String
) : Parcelable

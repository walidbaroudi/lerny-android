package com.comp491.domain.model.meeting

data class Receiver(
    val name: String,
    val userId: String
)

package com.comp491.domain.model.error

import com.comp491.domain.utils.toSafeString

sealed class LernyError(var customMessage: String? = null, cause: Throwable? = null, var trace: String? = null) : Exception(customMessage, cause) {

    object UnknownError : LernyError("Sorry, Something went wrong")

    data class OtherError(val errorMessage: String?, val errorCause: Throwable? = null) : LernyError(errorMessage, errorCause)

    fun safeMessage() = customMessage.toSafeString()
}
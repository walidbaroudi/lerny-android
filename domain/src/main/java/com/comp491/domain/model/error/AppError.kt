package com.comp491.domain.model.error

sealed class AppError(message: String? = null, cause: Throwable? = null) : LernyError(message, cause) {


    object NoUserFoundError : AppError("No user found!")
    object CorruptedMeetingError : AppError("Could not join meeting")
    object NullStatisticsError : AppError("Could not get statistics")
    object NoStatisticsError : AppError("No statistics yet")

    override fun toString(): String = super.message ?: super.toString()
}
package com.comp491.domain.model.session

import android.os.Parcelable
import com.comp491.domain.model.general.ObjectId
import com.comp491.domain.model.general.Score
import com.comp491.domain.model.meeting.Question
import com.comp491.domain.model.user.User
import com.comp491.domain.utils.now
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class Meeting(
    @SerializedName("_id") val meetingId: ObjectId,
    @SerializedName("meeting_title") val meetingTitle: String,
    val tutor: User,
    val student: User,
    @SerializedName("start_time") val startTime: Long,
    @SerializedName("end_time") val endTime: Long,
    @SerializedName("meeting_endpoint") val endpoint: String? = null,
    val questions: List<Question> = listOf(),
    val score: Int? = null
) : Parcelable {
    @IgnoredOnParcel
    val time
        get() = TimeSlot(startTime, endTime)

    fun getId() = meetingId.id

    fun studentCanJoin(): Boolean = now() in startTime..endTime

    fun getLengthInMinutes() = time.lengthInMinutes()

    fun isPast() = startTime < now()

    fun getScore() = score?.let { Score(student.email, getId(), it) }
}
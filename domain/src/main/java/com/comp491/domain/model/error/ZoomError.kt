package com.comp491.domain.model.error

sealed class ZoomError(cause: String, val code: Int? = -1): AppError(cause) {

    data class ZoomGeneralError(val mCause: String, val mCode: Int? = -1) : ZoomError(mCause, mCode)
    object ZoomSessionNotStarted : ZoomError("Please wait for your tutor to start the meeting")
    object ZoomBlankToken: ZoomError("Blank token")
}
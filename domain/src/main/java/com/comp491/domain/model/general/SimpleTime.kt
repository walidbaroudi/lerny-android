package com.comp491.domain.model.general

import com.comp491.domain.utils.asMinTwoDigitString

data class SimpleTime(
    val hour: Int,
    val minute: Int
) {
    fun getTimeString() = "${hour.asMinTwoDigitString()}:${minute.asMinTwoDigitString()}"
}

package com.comp491.domain.model.meeting

import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName


data class InteractionMessage(
    @SerializedName("message") val interaction: MeetingInteraction,
    val receiver: Receiver
)


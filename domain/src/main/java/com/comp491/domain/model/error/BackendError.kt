package com.comp491.domain.model.error

sealed class BackendError(message: String? = null, cause: Throwable? = null) : LernyError(message, cause) {


    object NoError : BackendError("Success")
    object InvalidCredentialsError : BackendError("No account found for the given email and password")

    override fun toString(): String = super.message ?: super.toString()
}

enum class BackendErrorEnum(private val code: Int, private val error: BackendError) {
    NO_ERROR(200, BackendError.NoError),
    INCORRECT_PASSWORD(1001, BackendError.InvalidCredentialsError),
    USER_NOT_FOUND(1002, BackendError.InvalidCredentialsError);

    companion object {
        fun errorForCode(code: Int) = values().find { it.code == code }?.error
    }
}
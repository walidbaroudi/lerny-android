package com.comp491.domain.model.user

import android.os.Parcelable
import com.comp491.domain.model.general.Score
import com.comp491.domain.model.session.Meeting
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    @SerializedName("entity") val type: String,
    val email: String,
    val name: String,
    val surname: String,
    val gender: String,
    val meetings: List<Meeting>,
) : Parcelable {
    fun getType() = UserType.typeForValue(type)
    fun isStudent() = getType() == UserType.USER_STUDENT
    fun isTutor() = getType() == UserType.USER_TUTOR
    fun getFullName() = "$name $surname"
    fun isMale() = gender in listOf("m", "male", "Male")
    fun isFemale() = gender in listOf("f", "female", "Female")
}
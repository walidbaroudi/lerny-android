package com.comp491.domain.model.general

import com.comp491.domain.utils.asMinTwoDigitString

data class SimpleDate(
    val day: Int,
    val month: Int,
    val year: Int
) {
    fun getDateString() = "${day.asMinTwoDigitString()}/${(month + 1).asMinTwoDigitString()}/$year"
}

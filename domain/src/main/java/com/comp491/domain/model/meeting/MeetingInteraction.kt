package com.comp491.domain.model.meeting

import com.google.gson.JsonElement


data class MeetingInteraction(val code: Int, val payload: JsonElement)

enum class MeetingInteractionType {
    INTERACTION_QUESTION,
    INTERACTION_ANSWER,
    INTERACTION_EVAL
}

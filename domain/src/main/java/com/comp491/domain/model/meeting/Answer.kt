package com.comp491.domain.model.meeting

import com.google.gson.annotations.SerializedName

data class Answer(
    @SerializedName("answer") val index: Int,
)
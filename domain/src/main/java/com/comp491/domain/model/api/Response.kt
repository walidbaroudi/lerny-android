package com.comp491.domain.model.api

import com.comp491.domain.model.error.BackendErrorEnum
import com.comp491.domain.model.error.LernyError
import com.google.gson.annotations.SerializedName


data class Response<T>(
    @SerializedName("body") val data: T?,
    @SerializedName("Message") val messagee: String = "",
    @SerializedName("status code") val code: Int = 200,
    private val error: LernyError? = BackendErrorEnum.errorForCode(code) ?: LernyError.OtherError(messagee)
) {
    fun getError() = error ?: BackendErrorEnum.errorForCode(code) ?: LernyError.OtherError(messagee)
}

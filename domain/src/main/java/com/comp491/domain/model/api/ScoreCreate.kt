package com.comp491.domain.model.api

import com.google.gson.annotations.SerializedName

data class ScoreCreate(
    val score: Int,
    @SerializedName("meeting_id") val meetingId: String
)

package com.comp491.domain.model.api

data class AuthRequest(
    val email: String,
    val password: String
)

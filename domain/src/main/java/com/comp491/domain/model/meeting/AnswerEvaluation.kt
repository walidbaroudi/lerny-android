package com.comp491.domain.model.meeting

data class AnswerEvaluation(
    val isCorrect: Boolean,
)
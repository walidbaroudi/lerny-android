package com.comp491.domain.model.meeting

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
enum class QuestionType(private val value: String) : Parcelable {
    MC("Multi-choice"),
    TF("True / False");

    fun isMultiChoice() = this == MC
    fun isTrueFalse() = this == TF

    companion object {
        fun forName(name: String) = valueOf(name)
        fun forValue(value: String) = values().firstOrNull { it.value == value }
    }
}
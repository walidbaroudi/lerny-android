package com.comp491.domain.model.api

data class TokenRequest(val topic: String, val role: Int)

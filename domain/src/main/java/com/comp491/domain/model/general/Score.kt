package com.comp491.domain.model.general

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Score(
    @SerializedName("email") val studentEmail: String,
    @SerializedName("meeting_id") val meetingId: String,
    @SerializedName("correct_percentage") val scorePercentage: Int
) : Parcelable {
    fun getPercentageString() = "$scorePercentage%"
}
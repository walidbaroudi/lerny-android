package com.comp491.domain.model.meeting

import android.os.Parcelable
import com.comp491.domain.utils.toBoolean
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Question(
    val type: QuestionType,
    @SerializedName("question") val questionText: String,
    val answers: List<String>,
    val time: Int
) : Parcelable {
    fun getQuestionTimeMillis() = time * 1000L
    fun getAnswer(index: Int) = if (type.isMultiChoice()) index.takeIf { it in 0..4 }?.let { answers[it] } else index.toBoolean().toString()
}

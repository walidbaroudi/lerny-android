package com.comp491.domain.model.user

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
enum class UserType(val value: String) : Parcelable {
    USER_STUDENT("student"),
    USER_TUTOR("tutor");

    companion object {
        fun typeForValue(value: String) = values().firstOrNull { it.value.equals(value, true) }
    }
}
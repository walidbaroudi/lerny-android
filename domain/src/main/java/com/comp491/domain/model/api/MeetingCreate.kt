package com.comp491.domain.model.api

import android.os.Parcelable
import com.comp491.domain.model.meeting.Question
import com.comp491.domain.model.session.TimeSlot
import com.comp491.domain.utils.now
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class MeetingCreate(
    @SerializedName("meeting_title") val meetingTitle: String,
    @SerializedName("tutor_email") val tutorEmail: String,
    @SerializedName("student_email") val studentEmail: String,
    @SerializedName("start_time") val startTime: Long,
    @SerializedName("end_time") val endTime: Long,
    val questions: List<Question> = listOf()
) : Parcelable {
    @IgnoredOnParcel
    val time
        get() = TimeSlot(startTime, endTime)

    fun studentCanJoin(): Boolean = now() in startTime..endTime
}
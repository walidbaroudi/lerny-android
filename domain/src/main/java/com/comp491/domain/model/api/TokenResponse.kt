package com.comp491.domain.model.api

data class TokenResponse(val signature: String)

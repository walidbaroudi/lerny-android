package com.comp491.domain.model.session

import android.os.Parcelable
import com.comp491.domain.utils.*
import kotlinx.parcelize.Parcelize

@Parcelize
data class TimeSlot(
    val startDate: Long,
    val endDate: Long
) : Parcelable {
    fun getTimeRangeString(): String {
        val startHour = getHour(startDate)
        val startMinute = getMinute(startDate)
        val endHour = getHour(endDate)
        val endMinute = getMinute(endDate)

        return "${startHour.asMinTwoDigitString()}:${startMinute.asMinTwoDigitString()} - ${endHour.asMinTwoDigitString()}:${endMinute.asMinTwoDigitString()}"
    }

    fun getWeekDay() = getDayOfWeekName(startDate)
    fun getDayString() = when (millisToDays(now() - endDate)) {
        0L -> "Today"
        1L -> "Yesterday"
        -1L -> "Tomorrow"
        else -> getDateString(endDate)
    }

    fun lengthInMinutes() = millisToMinutes(endDate - startDate)

}

package com.comp491.data

import android.os.Parcelable
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import com.comp491.domain.utils.jsonToObject
import com.comp491.domain.utils.toJson
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking

// put
suspend fun DataStore<Preferences>.putString(key: String, value: String) = put(stringPreferencesKey(key), value)
suspend fun DataStore<Preferences>.putInt(key: String, value: Int) = put(intPreferencesKey(key), value)
suspend fun DataStore<Preferences>.putBoolean(key: String, value: Boolean) = put(booleanPreferencesKey(key), value)
suspend fun DataStore<Preferences>.putLong(key: String, value: Long) = put(longPreferencesKey(key), value)
suspend fun DataStore<Preferences>.putFloat(key: String, value: Float) = put(floatPreferencesKey(key), value)
suspend fun DataStore<Preferences>.putDouble(key: String, value: Double) = put(doublePreferencesKey(key), value)
suspend fun DataStore<Preferences>.putStringSet(key: String, value: Set<String>) = put(stringSetPreferencesKey(key), value)
suspend fun DataStore<Preferences>.putParcelable(key: String, value: Parcelable) = put(stringPreferencesKey(key), value.toJson())

// get
suspend fun DataStore<Preferences>.getString(key: String) = get(stringPreferencesKey(key))
suspend fun DataStore<Preferences>.getInt(key: String) = get(intPreferencesKey(key))
suspend fun DataStore<Preferences>.getBoolean(key: String) = get(booleanPreferencesKey(key))
suspend fun DataStore<Preferences>.getLong(key: String) = get(longPreferencesKey(key))
suspend fun DataStore<Preferences>.getFloat(key: String) = get(floatPreferencesKey(key))
suspend fun DataStore<Preferences>.getDouble(key: String) = get(doublePreferencesKey(key))
suspend fun DataStore<Preferences>.getStringSet(key: String) = get(stringSetPreferencesKey(key))
suspend inline fun <reified T> DataStore<Preferences>.getParcelable(key: String) = get(stringPreferencesKey(key))?.jsonToObject<T>()

// clear

fun DataStore<Preferences>.clear() =
    runBlocking {
        edit { it.clear() }
        Unit
    }

private suspend fun <T> DataStore<Preferences>.put(key: Preferences.Key<T>, value: T) {
    edit { store -> store[key] = value }
}

suspend inline fun <reified T> DataStore<Preferences>.get(key: Preferences.Key<T>) = data.first()[key]


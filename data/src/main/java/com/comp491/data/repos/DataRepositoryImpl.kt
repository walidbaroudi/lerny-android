package com.comp491.data.repos

import com.comp491.data.backend.service.LernyBackendService
import com.comp491.data.local.LocalStorage
import com.comp491.domain.model.api.*
import com.comp491.domain.model.error.AppError
import com.comp491.domain.model.error.LernyError
import com.comp491.domain.model.general.Score
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.model.user.User
import com.comp491.domain.repos.DataRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

class DataRepositoryImpl(
    private val backendService: LernyBackendService,
    private val localStorage: LocalStorage
) : DataRepository() {

    override fun clearLocalData() {
        localStorage.clear()
    }

    // User

    override fun registerUser(register: UserRegister): Flow<Response<User>> {
        return backendService.registerUser(register)
    }


    override fun loginStudent(email: String, password: String): Flow<Response<User>> {
        return backendService.loginUser(email, password)
    }


    override fun setUser(user: User?) {
        localStorage.setUser(user)
    }

    override fun getUser() = localStorage.getUser()

    override fun isLoggedIn() = localStorage.isLoggedIn()

    override fun getUserMeetings(): Flow<Response<List<Meeting>>> {
        val user = getUser() ?: return getErrorFlow(AppError.NoUserFoundError)
        return if (user.isTutor())
            backendService.getTutorMeetings(user.email)
        else
            backendService.getStudentMeetings(user.email)
    }

    override fun getUserScores(): Flow<Response<List<Score>>> {
        return getUserMeetings().map {
            val scores = it.data?.mapNotNull { meeting -> meeting.getScore() }
            Response(scores, it.messagee, it.code, it.getError())
        }
    }

    override fun submitScore(score: ScoreCreate): Flow<Response<BlankData>> {
        return backendService.submitScore(score)
    }

    override fun createMeeting(meeting: MeetingCreate): Flow<Response<Meeting>> {
        return backendService.createMeeting(meeting)
    }

    override fun getStudentScores(): List<Score> {
//        return localStorage.getStudentScores() // todo: implement when fixed on backend
        return listOf()
    }


    // Zoom
    override fun generateToken(topic: String, role: Int): Flow<Response<String>> =
        backendService.generateToken(topic, role)

    private fun <T> getErrorFlow(exception: LernyError): Flow<Response<T>> =
        flow { emit(Response(null, error = exception)) }
}

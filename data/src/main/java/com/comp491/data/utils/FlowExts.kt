package com.comp491.data.utils

import com.comp491.domain.utils.sendToCrashlytics
import kotlinx.coroutines.flow.*

fun <T> Flow<T>.doOnError(onError: (Throwable) -> Unit): Flow<T> {
    return flow {
        try {
            collect { value ->
                emit(value)
            }
        } catch (e: Exception) {
            e.sendToCrashlytics()
            onError(e)
        }
    }
}

fun <T> Flow<T>.convertError(): Flow<T> {
    return flow {
        try {
            collect { value ->
                emit(value)
            }
        } catch (e: Exception) {
            e.sendToCrashlytics()
            throw e
        }
    }
}

fun <T> MutableStateFlow<UiModelState<T>>.clear() {
    value = UiModelState.None
}
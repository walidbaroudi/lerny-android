package com.comp491.data.utils

import androidx.lifecycle.MutableLiveData
import com.comp491.domain.model.error.LernyError
import kotlinx.coroutines.flow.MutableStateFlow

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class UiModelState<out R> {

    data class Success<out T>(val data: T) : UiModelState<T>()
    data class Error(val exception: LernyError) : UiModelState<Nothing>()
    object Loading : UiModelState<Nothing>()
    object None : UiModelState<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
            is Loading -> "Loading"
            is None -> "Pending State"
        }
    }
}

/**
 * `true` if [UiModelState] is of type Success & holds non-null Success.data.
 */
val UiModelState<*>.succeeded
    get() = this is UiModelState.Success && data != null

fun <T> UiModelState<T>.successOr(fallback: T): T {
    return (this as? UiModelState.Success<T>)?.data ?: fallback
}

val <T> UiModelState<T>.data: T?
    get() = (this as? UiModelState.Success)?.data

/**
 * Updates value of [liveData] if [UiModelState] is of type Success
 */
inline fun <reified T> UiModelState<T>.updateOnSuccess(liveData: MutableLiveData<T>) {
    if (this is UiModelState.Success) {
        liveData.value = data
    }
}

/**
 * Updates value of [MutableStateFlow] if [UiModelState] is of type Success
 */
inline fun <reified T> UiModelState<T>.updateOnSuccess(stateFlow: MutableStateFlow<T>) {
    if (this is UiModelState.Success) {
        stateFlow.value = data
    }
}

package com.comp491.data.backend.api

import com.comp491.domain.model.api.*
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.model.user.User
import retrofit2.http.*

interface UserApi {

    @POST("user")
    suspend fun registerUser(@Body register: UserRegister): Response<User>

    @POST("login")
    suspend fun loginUser(@Body credentials: AuthRequest): Response<User>

    @GET("user")
    suspend fun getUser(@Query("email") email: String): Response<User>

    @GET("users")
    suspend fun getAllUsers(@Query("email") email: String): Response<List<User>>

    @POST("users-by-emails")
    suspend fun getUsersByEmails(@Body emails: List<String>): Response<List<User>>

    @GET("meetings")
    suspend fun getStudentMeetings(@Query("student_email") email: String): Response<List<Meeting>>

    @GET("meetings")
    suspend fun getTutorMeetings(@Query("tutor_email") email: String): Response<List<Meeting>>

}
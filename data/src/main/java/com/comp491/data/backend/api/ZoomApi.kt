package com.comp491.data.backend.api

import com.comp491.domain.model.api.TokenRequest
import com.comp491.domain.model.api.TokenResponse
import com.comp491.domain.model.user.User
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

interface ZoomApi {
    @POST("get-video-token/")
    suspend fun generateToken(@Body userId: TokenRequest): TokenResponse
}
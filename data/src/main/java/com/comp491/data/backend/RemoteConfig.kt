package com.comp491.data.backend

import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings

object RemoteConfig {
    private val DEFAULTS: HashMap<String, Any> =
        hashMapOf(
            ConfigKeys.KEY_API_URL to ConfigValues.VAL_API_URL
        )

    fun init() {
        val configSettings = remoteConfigSettings { minimumFetchIntervalInSeconds = 0 }
        Firebase.remoteConfig.apply {
            setConfigSettingsAsync(configSettings)
            setDefaultsAsync(DEFAULTS)
            fetchAndActivate().addOnCompleteListener { if (it.isSuccessful) activate() }
        }
    }

    fun getApiUrl(): String = Firebase.remoteConfig.getString(ConfigKeys.KEY_API_URL)

    private object ConfigKeys {
        const val KEY_API_URL = "KEY_API_URL"
    }

    private object ConfigValues {
        const val VAL_API_URL = "http://ec2-34-204-69-176.compute-1.amazonaws.com:5000/"
    }
}
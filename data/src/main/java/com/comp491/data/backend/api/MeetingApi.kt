package com.comp491.data.backend.api

import com.comp491.domain.model.api.*
import com.comp491.domain.model.session.Meeting
import retrofit2.http.*

interface MeetingApi {

    @POST("meeting")
    suspend fun createMeeting(@Body meeting: MeetingCreate): Response<Meeting>

    @POST("add-student-answer")
    suspend fun submitScore(@Body score: ScoreCreate): Response<BlankData>
}
package com.comp491.data.backend

import com.comp491.data.backend.api.MeetingApi
import com.comp491.data.backend.api.UserApi
import com.comp491.data.backend.api.ZoomApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitClient(private val url: String) {

    private fun provideRetrofit(apiUrl: String = url): Retrofit {
        val client = OkHttpClient.Builder()
            .writeTimeout(2, TimeUnit.MINUTES)
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(2, TimeUnit.MINUTES)
            .build()

        return Retrofit.Builder()
            .baseUrl(apiUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    fun getUserClient(): UserApi = provideRetrofit().create(UserApi::class.java)
    fun getMeetingClient(): MeetingApi = provideRetrofit().create(MeetingApi::class.java)
    fun getZoomClient(): ZoomApi = provideRetrofit("https://2k6v1j5wl0.execute-api.us-east-1.amazonaws.com/dev/").create(ZoomApi::class.java)
}
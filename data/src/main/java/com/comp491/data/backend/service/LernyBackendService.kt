package com.comp491.data.backend.service

import com.comp491.data.backend.api.MeetingApi
import com.comp491.data.backend.api.UserApi
import com.comp491.data.backend.api.ZoomApi
import com.comp491.data.utils.convertError
import com.comp491.domain.model.api.*
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.model.user.User
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

class LernyBackendService(
    private val userClient: UserApi,
    private val meetingClient: MeetingApi,
    private val zoomApi: ZoomApi,

    ) {


    fun registerUser(register: UserRegister): Flow<Response<User>> {
        return flow {
            val result = userClient.registerUser(register)
            emit(result)
        }.convertError()
    }

    fun generateToken(topic: String, role: Int): Flow<Response<String>> {
        return flow {
            val result = zoomApi.generateToken(TokenRequest(topic, role))
            emit(result)
        }.convertError()
            .map { Response(it.signature) }
    }

    fun loginUser(email: String, password: String): Flow<Response<User>> {
        val request = AuthRequest(email, password)
        return flow {
            val result = userClient.loginUser(request)
            emit(result)
        }.convertError()
    }

    fun getUser(email: String): Flow<Response<User>> {
        return flow {
            val result = userClient.getUser(email)
            emit(result)
        }.convertError()
    }

    fun getAllUsers(email: String): Flow<Response<List<User>>> {
        return flow {
            val result = userClient.getAllUsers(email)
            emit(result)
        }.convertError()
    }


    fun createMeeting(meeting: MeetingCreate): Flow<Response<Meeting>> {
        return flow {
            val result = meetingClient.createMeeting(meeting)
            emit(result)
        }.convertError()
    }

    fun submitScore(score: ScoreCreate): Flow<Response<BlankData>> {
        return flow {
            val result = meetingClient.submitScore(score)
            emit(result)
        }.convertError()
    }

    fun getTutorMeetings(email: String): Flow<Response<List<Meeting>>> {
        return flow {
            val result = userClient.getTutorMeetings(email)
            emit(result)
        }.convertError()
    }

    fun getStudentMeetings(email: String): Flow<Response<List<Meeting>>> {
        return flow {
            val result = userClient.getStudentMeetings(email)
            emit(result)
        }.convertError()
    }

}
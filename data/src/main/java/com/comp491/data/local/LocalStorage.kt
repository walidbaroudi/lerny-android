package com.comp491.data.local

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.comp491.data.clear
import com.comp491.data.getParcelable
import com.comp491.domain.utils.runAsync
import com.comp491.data.putParcelable
import com.comp491.domain.model.session.Meeting
import com.comp491.domain.model.user.User
import com.comp491.domain.model.user.UserType
import com.comp491.domain.utils.notNull
import kotlinx.coroutines.runBlocking

class LocalStorage(private val store: DataStore<Preferences>) {

    fun setUser(user: User?) = user?.let {
        runAsync { store.putParcelable(KEY_USER, it) }
    }


    fun getUser(): User? { // todo: make async maybe?
        return runBlocking { store.getParcelable(KEY_USER) }
    }

    fun isUserStudent() = getUser()?.getType() == UserType.USER_STUDENT
    fun isUserTutor() = getUser()?.getType() == UserType.USER_TUTOR

    fun isLoggedIn() = runBlocking { store.getParcelable<User>(KEY_USER) }.notNull()

    fun getUserMeetings(): List<Meeting> {
        return getUser()!!.meetings
    }

//    fun getStudentScores() = (getUser() as Student).scores // todo: implement when fixed on backend

    fun clear() = store.clear()

    companion object {
        // store keys
        private const val KEY_USER = "CURRENT_USER"
    }

}